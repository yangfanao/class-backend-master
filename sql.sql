/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50087
 Source Host           : localhost:3306
 Source Schema         : his

 Target Server Type    : MySQL
 Target Server Version : 50087
 File Encoding         : 65001

 Date: 20/10/2022 11:16:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cms_channels
-- ----------------------------
DROP TABLE IF EXISTS `cms_channels`;
CREATE TABLE `cms_channels`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `guid` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'GUID',
  `extendValues` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '扩展内容',
  `channelName` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '频道名称',
  `siteId` int(11) NOT NULL COMMENT '所属站点',
  `contentModelPluginId` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容插件ID',
  `tableName` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '关联表名',
  `parentId` int(11) NOT NULL COMMENT '父级ID',
  `parentsPath` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '父级路径',
  `parentsCount` int(11) NOT NULL COMMENT '父级数量',
  `childrenCount` int(11) NOT NULL COMMENT '子级数量',
  `indexName` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '排序名称',
  `groupNames` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '分组',
  `imageUrl` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '图片',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容描述',
  `filePath` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '文件路径',
  `channelFilePathRule` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '频道路径路径规则',
  `contentFilePathRule` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容文件路径规则',
  `linkUrl` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '外链',
  `linkType` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '外链类型',
  `channelTemplteId` int(11) NOT NULL COMMENT '频道模版',
  `contentTemplateId` int(11) NOT NULL COMMENT '内容模版',
  `keywords` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '关键字',
  `description` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '描述',
  `OrderNum` int(11) NOT NULL COMMENT '排序',
  `CreationTime` datetime NOT NULL COMMENT '创建时间',
  `CreatorUserId` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `LastModificationTime` datetime NULL DEFAULT NULL COMMENT '最后修改时间',
  `LastModifierUserId` bigint(20) NULL DEFAULT NULL COMMENT '最后修改人',
  `IsDeleted` tinyint(1) NOT NULL COMMENT '是否删除',
  `DeleterUserId` bigint(20) NULL DEFAULT NULL COMMENT '删除人',
  `DeletionTime` datetime NULL DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY USING BTREE (`Id`),
  INDEX `IX_Channels_siteId` USING BTREE(`siteId`),
  CONSTRAINT `FK_Channels_Sites_siteId` FOREIGN KEY (`siteId`) REFERENCES `cms_sites` (`Id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '频道表; InnoDB free: 11264 kB; (`siteId`) REFER `his/cms_sites`(`Id`) ON DELETE CAS' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cms_sites
-- ----------------------------
DROP TABLE IF EXISTS `cms_sites`;
CREATE TABLE `cms_sites`  (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `guid` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'GUID',
  `extendValues` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '扩展内容',
  `siteDir` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '站点目录',
  `siteName` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '站点名称',
  `siteType` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '站点类型',
  `imageUrl` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '站点图片',
  `keywords` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '关键字',
  `description` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '站点描述',
  `tableName` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '关联表名',
  `root` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否根目录',
  `parentId` int(11) NOT NULL COMMENT '上级站点',
  `OrderNum` int(11) NOT NULL COMMENT '顺序',
  `CreationTime` datetime NOT NULL COMMENT '创建时间',
  `CreatorUserId` bigint(20) NULL DEFAULT NULL COMMENT '创建人',
  `LastModificationTime` datetime NULL DEFAULT NULL COMMENT '最后修改时间',
  `LastModifierUserId` bigint(20) NULL DEFAULT NULL COMMENT '最后修改人',
  `IsDeleted` tinyint(1) NOT NULL COMMENT '是否删除',
  `DeleterUserId` bigint(20) NULL DEFAULT NULL COMMENT '删除人',
  `DeletionTime` datetime NULL DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY USING BTREE (`Id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '站点表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for factory_info
-- ----------------------------
DROP TABLE IF EXISTS `factory_info`;
CREATE TABLE `factory_info`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `factory_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '厂家名称',
  `factory_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '厂家编码',
  `contact` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系人',
  `tel` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电话',
  `keyword` varchar(35) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关键词',
  `status` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '状态',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `is_delete` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否删除',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY USING BTREE (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '生产厂家维护表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of factory_info
-- ----------------------------
INSERT INTO `factory_info` VALUES (1, '云南白药集团', '000538', 'ADC', '18099876656', 'yyds', '1', '2022-10-15 23:48:04', '2022-10-19 11:24:33', NULL, 'admin', NULL, '云南省');
INSERT INTO `factory_info` VALUES (2, '上海医药（集团）有限公司', '112313', '阿雷', '0551-23232323', '下次', '0', '2022-10-15 23:48:16', '2022-10-16 12:00:18', NULL, NULL, NULL, '上海市');
INSERT INTO `factory_info` VALUES (4, '哈药集团有限公司', '141231', NULL, '19023444433', '哈药', '0', '2022-10-20 10:13:01', NULL, 'admin', NULL, NULL, '哈尔滨');

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY USING BTREE (`table_id`)
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成业务表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES (5, 'factory_info', '生产厂家维护表; InnoDB free: 11264 kB', NULL, NULL, 'FactoryInfo', 'crud', 'com.ruoyi.project.cms', 'cms', 'info', '生产厂家维护; InnoDB free: 11264 kB', 'ruoyi', '0', '/', NULL, 'admin', '2022-10-18 20:59:55', '', NULL, NULL);

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY USING BTREE (`column_id`)
) ENGINE = InnoDB AUTO_INCREMENT = 62 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES (49, '5', 'id', 'id', 'bigint(20)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2022-10-18 20:59:55', '', NULL);
INSERT INTO `gen_table_column` VALUES (50, '5', 'factory_name', '厂家名称', 'varchar(64)', 'String', 'factoryName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', '2022-10-18 20:59:55', '', NULL);
INSERT INTO `gen_table_column` VALUES (51, '5', 'factory_code', '厂家编码', 'varchar(64)', 'String', 'factoryCode', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', '2022-10-18 20:59:55', '', NULL);
INSERT INTO `gen_table_column` VALUES (52, '5', 'contact', '联系人', 'varchar(64)', 'String', 'contact', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', '2022-10-18 20:59:55', '', NULL);
INSERT INTO `gen_table_column` VALUES (53, '5', 'tel', '电话', 'varchar(64)', 'String', 'tel', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2022-10-18 20:59:55', '', NULL);
INSERT INTO `gen_table_column` VALUES (54, '5', 'keyword', '关键词', 'varchar(35)', 'String', 'keyword', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', '2022-10-18 20:59:55', '', NULL);
INSERT INTO `gen_table_column` VALUES (55, '5', 'status', '状态', 'varchar(10)', 'String', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'radio', '', 7, 'admin', '2022-10-18 20:59:55', '', NULL);
INSERT INTO `gen_table_column` VALUES (56, '5', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 8, 'admin', '2022-10-18 20:59:55', '', NULL);
INSERT INTO `gen_table_column` VALUES (57, '5', 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 9, 'admin', '2022-10-18 20:59:55', '', NULL);
INSERT INTO `gen_table_column` VALUES (58, '5', 'create_by', '创建人', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 10, 'admin', '2022-10-18 20:59:55', '', NULL);
INSERT INTO `gen_table_column` VALUES (59, '5', 'update_by', '修改人', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 11, 'admin', '2022-10-18 20:59:55', '', NULL);
INSERT INTO `gen_table_column` VALUES (60, '5', 'is_delete', '是否删除', 'varchar(10)', 'String', 'isDelete', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 12, 'admin', '2022-10-18 20:59:55', '', NULL);
INSERT INTO `gen_table_column` VALUES (61, '5', 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'textarea', '', 13, 'admin', '2022-10-18 20:59:55', '', NULL);

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `blob_data` blob NULL COMMENT '存放持久化Trigger对象',
  PRIMARY KEY USING BTREE (`sched_name`, `trigger_name`, `trigger_group`),
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'Blob类型的触发器表; InnoDB free: 11264 kB; (`sched_name` `trigger_name` `trigger_group`' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `calendar_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '日历名称',
  `calendar` blob NOT NULL COMMENT '存放持久化calendar对象',
  PRIMARY KEY USING BTREE (`sched_name`, `calendar_name`)
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '日历信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `cron_expression` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'cron表达式',
  `time_zone_id` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '时区',
  PRIMARY KEY USING BTREE (`sched_name`, `trigger_name`, `trigger_group`),
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'Cron类型的触发器表; InnoDB free: 11264 kB; (`sched_name` `trigger_name` `trigger_group`' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', '0/10 * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', '0/15 * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', '0/20 * * * * ?', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `entry_id` varchar(95) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度器实例id',
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `instance_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度器实例名',
  `fired_time` bigint(13) NOT NULL COMMENT '触发的时间',
  `sched_time` bigint(13) NOT NULL COMMENT '定时器制定的时间',
  `priority` int(11) NOT NULL COMMENT '优先级',
  `state` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态',
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务名称',
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务组名',
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否并发',
  `requests_recovery` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否接受恢复执行',
  PRIMARY KEY USING BTREE (`sched_name`, `entry_id`)
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '已触发的触发器表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务组名',
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '相关介绍',
  `job_class_name` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '执行任务类名称',
  `is_durable` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否持久化',
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否并发',
  `is_update_data` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否更新数据',
  `requests_recovery` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '是否接受恢复执行',
  `job_data` blob NULL COMMENT '存放持久化job对象',
  PRIMARY KEY USING BTREE (`sched_name`, `job_name`, `job_group`)
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '任务详细信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 'com.ruoyi.common.utils.job.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F5045525449455373720027636F6D2E72756F79692E70726F6A6563742E6D6F6E69746F722E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720029636F6D2E72756F79692E6672616D65776F726B2E7765622E646F6D61696E2E42617365456E7469747900000000000000010200084C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0008697344656C65746571007E00094C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B5974190300007870770800000183D59417E07870707400007070707400013174000E302F3130202A202A202A202A203F74001172795461736B2E72794E6F506172616D7374000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000001740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E697A0E58F82EFBC8974000133740001317800);
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 'com.ruoyi.common.utils.job.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F5045525449455373720027636F6D2E72756F79692E70726F6A6563742E6D6F6E69746F722E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720029636F6D2E72756F79692E6672616D65776F726B2E7765622E646F6D61696E2E42617365456E7469747900000000000000010200084C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0008697344656C65746571007E00094C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B5974190300007870770800000183D59417E07870707400007070707400013174000E302F3135202A202A202A202A203F74001572795461736B2E7279506172616D7328277279272974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000002740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E69C89E58F82EFBC8974000133740001317800);
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 'com.ruoyi.common.utils.job.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F5045525449455373720027636F6D2E72756F79692E70726F6A6563742E6D6F6E69746F722E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720029636F6D2E72756F79692E6672616D65776F726B2E7765622E646F6D61696E2E42617365456E7469747900000000000000010200084C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0008697344656C65746571007E00094C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B5974190300007870770800000183D59417E07870707400007070707400013174000E302F3230202A202A202A202A203F74003872795461736B2E72794D756C7469706C65506172616D7328277279272C20747275652C20323030304C2C203331362E3530442C203130302974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000003740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E5A49AE58F82EFBC8974000133740001317800);

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `lock_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '悲观锁名称',
  PRIMARY KEY USING BTREE (`sched_name`, `lock_name`)
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '存储的悲观锁信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('RuoyiScheduler', 'STATE_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('RuoyiScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  PRIMARY KEY USING BTREE (`sched_name`, `trigger_group`)
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '暂停的触发器表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `instance_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '实例名称',
  `last_checkin_time` bigint(13) NOT NULL COMMENT '上次检查时间',
  `checkin_interval` bigint(13) NOT NULL COMMENT '检查间隔时间',
  PRIMARY KEY USING BTREE (`sched_name`, `instance_name`)
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '调度器状态表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
INSERT INTO `qrtz_scheduler_state` VALUES ('RuoyiScheduler', 'LAPTOP-OT9A785G1666180939332', 1666235786623, 15000);

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `repeat_count` bigint(7) NOT NULL COMMENT '重复的次数统计',
  `repeat_interval` bigint(12) NOT NULL COMMENT '重复的间隔时间',
  `times_triggered` bigint(10) NOT NULL COMMENT '已经触发的次数',
  PRIMARY KEY USING BTREE (`sched_name`, `trigger_name`, `trigger_group`),
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '简单触发器的信息表; InnoDB free: 11264 kB; (`sched_name` `trigger_name` `trigger_group`) ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `str_prop_1` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第一个参数',
  `str_prop_2` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第二个参数',
  `str_prop_3` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'String类型的trigger的第三个参数',
  `int_prop_1` int(11) NULL DEFAULT NULL COMMENT 'int类型的trigger的第一个参数',
  `int_prop_2` int(11) NULL DEFAULT NULL COMMENT 'int类型的trigger的第二个参数',
  `long_prop_1` bigint(20) NULL DEFAULT NULL COMMENT 'long类型的trigger的第一个参数',
  `long_prop_2` bigint(20) NULL DEFAULT NULL COMMENT 'long类型的trigger的第二个参数',
  `dec_prop_1` decimal(13, 4) NULL DEFAULT NULL COMMENT 'decimal类型的trigger的第一个参数',
  `dec_prop_2` decimal(13, 4) NULL DEFAULT NULL COMMENT 'decimal类型的trigger的第二个参数',
  `bool_prop_1` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Boolean类型的trigger的第一个参数',
  `bool_prop_2` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Boolean类型的trigger的第二个参数',
  PRIMARY KEY USING BTREE (`sched_name`, `trigger_name`, `trigger_group`),
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '同步机制的行锁表; InnoDB free: 11264 kB; (`sched_name` `trigger_name` `trigger_group`) R' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '触发器的名字',
  `trigger_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '触发器所属组的名字',
  `job_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_job_details表job_name的外键',
  `job_group` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'qrtz_job_details表job_group的外键',
  `description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '相关介绍',
  `next_fire_time` bigint(13) NULL DEFAULT NULL COMMENT '上一次触发时间（毫秒）',
  `prev_fire_time` bigint(13) NULL DEFAULT NULL COMMENT '下一次触发时间（默认为-1表示不触发）',
  `priority` int(11) NULL DEFAULT NULL COMMENT '优先级',
  `trigger_state` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '触发器状态',
  `trigger_type` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '触发器的类型',
  `start_time` bigint(13) NOT NULL COMMENT '开始时间',
  `end_time` bigint(13) NULL DEFAULT NULL COMMENT '结束时间',
  `calendar_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日程表名称',
  `misfire_instr` smallint(2) NULL DEFAULT NULL COMMENT '补偿执行的策略',
  `job_data` blob NULL COMMENT '存放持久化job对象',
  PRIMARY KEY USING BTREE (`sched_name`, `trigger_name`, `trigger_group`),
  INDEX `sched_name` USING BTREE(`sched_name`, `job_name`, `job_group`),
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `qrtz_job_details` (`sched_name`, `job_name`, `job_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '触发器详细信息表; InnoDB free: 11264 kB; (`sched_name` `job_name` `job_group`) REFER `hi' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 1666180940000, -1, 5, 'PAUSED', 'CRON', 1666180939000, 0, NULL, 2, '');
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 1666180950000, -1, 5, 'PAUSED', 'CRON', 1666180939000, 0, NULL, 2, '');
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 1666180940000, -1, 5, 'PAUSED', 'CRON', 1666180939000, 0, NULL, 2, '');

-- ----------------------------
-- Table structure for red_captcha
-- ----------------------------
DROP TABLE IF EXISTS `red_captcha`;
CREATE TABLE `red_captcha`  (
  `captcha_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '验证码id',
  `captcha_key` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '验证码Key',
  `captcha_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '验证码',
  `captcha_expiration` int(4) NULL DEFAULT 2 COMMENT '验证码有效期',
  `captcha_util` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '有效期单位',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `expreation_time` datetime NULL DEFAULT NULL COMMENT '超期时间',
  PRIMARY KEY USING BTREE (`captcha_id`)
) ENGINE = InnoDB AUTO_INCREMENT = 243 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '验证码表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of red_captcha
-- ----------------------------
INSERT INTO `red_captcha` VALUES (200, 'captcha_codes:7b088cb890db463aa3c097e7eb47087d', '0', 2, 'MINUTES', '', '2022-10-14 22:28:15', '2022-10-14 22:30:15');
INSERT INTO `red_captcha` VALUES (201, 'captcha_codes:8ff84f7a81764134bc6609b65feb1dad', '0', 2, 'MINUTES', '', '2022-10-14 22:28:19', '2022-10-14 22:30:19');
INSERT INTO `red_captcha` VALUES (207, 'captcha_codes:b9f198dce85c4e1592138dada620f8ef', '0', 2, 'MINUTES', '', '2022-10-16 09:53:28', '2022-10-16 09:55:28');
INSERT INTO `red_captcha` VALUES (209, 'captcha_codes:ef9251269d094bf08c574a4991d016af', '14', 2, 'MINUTES', '', '2022-10-16 09:56:26', '2022-10-16 09:58:26');
INSERT INTO `red_captcha` VALUES (212, 'captcha_codes:eedbabb46e074bdbb815976800958297', '28', 2, 'MINUTES', '', '2022-10-16 11:20:09', '2022-10-16 11:22:09');
INSERT INTO `red_captcha` VALUES (213, 'captcha_codes:54903d1ad7774926ae08151cac7eec71', '11', 2, 'MINUTES', '', '2022-10-16 11:21:40', '2022-10-16 11:23:40');
INSERT INTO `red_captcha` VALUES (214, 'captcha_codes:dfae6ef707c84555bbe46ce3eed3d8da', '14', 2, 'MINUTES', '', '2022-10-16 11:23:53', '2022-10-16 11:25:53');
INSERT INTO `red_captcha` VALUES (215, 'captcha_codes:7bdd65ac3a70446bbc499d945193baec', '7', 2, 'MINUTES', '', '2022-10-16 11:26:47', '2022-10-16 11:28:47');
INSERT INTO `red_captcha` VALUES (216, 'captcha_codes:404c066cdeb34dacaf24b9e926a2e312', '16', 2, 'MINUTES', '', '2022-10-16 11:27:38', '2022-10-16 11:29:38');
INSERT INTO `red_captcha` VALUES (217, 'captcha_codes:f2ea401aed5f4a9094d496c592718bb8', '0', 2, 'MINUTES', '', '2022-10-16 11:29:30', '2022-10-16 11:31:30');
INSERT INTO `red_captcha` VALUES (219, 'captcha_codes:cd8e1f6d8f634013af257bcc906ece83', '13', 2, 'MINUTES', '', '2022-10-16 16:36:55', '2022-10-16 16:38:55');
INSERT INTO `red_captcha` VALUES (222, 'captcha_codes:d1af5ed1febe4ab8b34d3194da499ad0', '30', 2, 'MINUTES', '', '2022-10-16 16:42:55', '2022-10-16 16:44:55');
INSERT INTO `red_captcha` VALUES (230, 'captcha_codes:bd256cb354dc42628e3b63b83600d4e5', '11', 2, 'MINUTES', '', '2022-10-18 14:47:34', '2022-10-18 14:49:34');
INSERT INTO `red_captcha` VALUES (232, 'captcha_codes:0ad659935b09429aabbdf60e2a60e2de', '0', 2, 'MINUTES', '', '2022-10-18 20:59:09', '2022-10-18 21:01:09');
INSERT INTO `red_captcha` VALUES (235, 'captcha_codes:090e75b81d304fa49c3e007f00897530', '6', 2, 'MINUTES', '', '2022-10-19 10:24:30', '2022-10-19 10:26:30');
INSERT INTO `red_captcha` VALUES (237, 'captcha_codes:27ab84eb3d124b6ab41eb24bfc22357b', '1', 2, 'MINUTES', '', '2022-10-19 10:47:58', '2022-10-19 10:49:58');
INSERT INTO `red_captcha` VALUES (241, 'captcha_codes:c563781ac3b34e3986d1c5d20d4be19f', '0', 2, 'MINUTES', '', '2022-10-20 10:11:58', '2022-10-20 10:13:58');

-- ----------------------------
-- Table structure for red_user
-- ----------------------------
DROP TABLE IF EXISTS `red_user`;
CREATE TABLE `red_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '缓存ID',
  `reduser_id` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户ID',
  `user_key` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '缓存KEY',
  `sysuser` blob NULL COMMENT '缓存用户信息',
  `logininfo` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '缓存登录信息',
  `permissions` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '权限信息',
  `captcha_expiration` int(4) NULL DEFAULT 2 COMMENT '有效期',
  `captcha_util` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '有效期单位',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `expreation_time` datetime NULL DEFAULT NULL COMMENT '超期时间',
  PRIMARY KEY USING BTREE (`user_id`)
) ENGINE = InnoDB AUTO_INCREMENT = 200 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '登录用户表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of red_user
-- ----------------------------
INSERT INTO `red_user` VALUES (1, '', 'login_tokens:c70b113a-5469-472c-9f2f-f1f6e14d387e', 0x7B2261646D696E223A747275652C22617661746172223A22222C226372656174654279223A2261646D696E222C2263726561746554696D65223A313636353733353539363030302C2264656C466C6167223A2230222C2264657074223A7B226368696C6472656E223A5B5D2C22646570744964223A3130332C22646570744E616D65223A22E58685E7A791222C226C6561646572223A22222C226F726465724E756D223A2231222C22706172616D73223A7B7D2C22706172656E744964223A3130312C22737461747573223A2230227D2C22646570744964223A3130332C22656D61696C223A22323133403136332E636F6D222C226C6F67696E44617465223A313636363134373935353030302C226C6F67696E4970223A223132372E302E302E31222C226E69636B4E616D65223A22E69D8EE59B9B222C22706172616D73223A7B7D2C2270617373776F7264223A2224326124313024374A4237323079756256535A765549307245714B2F2E5671474F5A54482E756C75333364484F6942453842794F684A497264417532222C2270686F6E656E756D626572223A223135383838383838383838222C2272656D61726B223A22E7AEA1E79086E59198222C22726F6C6573223A5B7B2261646D696E223A747275652C226461746153636F7065223A2231222C2264657074436865636B5374726963746C79223A66616C73652C22666C6167223A66616C73652C226D656E75436865636B5374726963746C79223A66616C73652C22706172616D73223A7B7D2C22726F6C654964223A312C22726F6C654B6579223A2261646D696E222C22726F6C654E616D65223A22E8B685E7BAA7E7AEA1E79086E59198222C22726F6C65536F7274223A2231222C22737461747573223A2230227D5D2C22736578223A2231222C22737461747573223A2230222C22757365724964223A312C22757365724E616D65223A2261646D696E227D, '{\"browser\":\"Chrome\",\"deptId\":103,\"expireTime\":1666234106032,\"ipaddr\":\"127.0.0.1\",\"loginLocation\":\"内网IP\",\"loginTime\":1666232306032,\"os\":\"Windows 10\",\"token\":\"c70b113a-5469-472c-9f2f-f1f6e14d387e\",\"user\":{\"admin\":false,\"params\":{}},\"userId\":1}', '[\"*:*:*\"]', 0, NULL, 'sys', '2022-10-20 10:18:26', '2022-10-20 10:48:26');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY USING BTREE (`config_id`)
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '参数配置表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2022-10-14 16:19:56', '', NULL, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2022-10-14 16:19:56', '', NULL, '初始化密码 123456');
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2022-10-14 16:19:56', '', NULL, '深色主题theme-dark，浅色主题theme-light');
INSERT INTO `sys_config` VALUES (4, '账号自助-验证码开关', 'sys.account.captchaOnOff', 'true', 'Y', 'admin', '2022-10-14 16:19:56', '', NULL, '是否开启验证码功能（true开启，false关闭）');
INSERT INTO `sys_config` VALUES (5, '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'false', 'Y', 'admin', '2022-10-14 16:19:56', '', NULL, '是否开启注册用户功能（true开启，false关闭）');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY USING BTREE (`dept_id`)
) ENGINE = InnoDB AUTO_INCREMENT = 200 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', '此一级去掉', 0, '', '15888888888', '123@qq.com', '0', '0', 'admin', '2022-10-14 16:19:56', 'admin', '2022-10-20 09:47:21');
INSERT INTO `sys_dept` VALUES (101, 100, '0,100', '西院区', 1, '阿鲁', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-10-14 16:19:56', 'admin', '2022-10-20 09:47:40');
INSERT INTO `sys_dept` VALUES (102, 100, '0,100', '本部', 2, '阿鲁', '15888888888', '123@qq.com', '0', '0', 'admin', '2022-10-14 16:19:56', 'admin', '2022-10-20 09:47:56');
INSERT INTO `sys_dept` VALUES (103, 101, '0,100,101', '内科', 1, '', '15888888888', '123@qq.com', '0', '0', 'admin', '2022-10-14 16:19:56', 'admin', '2022-10-20 09:48:31');
INSERT INTO `sys_dept` VALUES (104, 101, '0,100,101', '外科', 2, '', '15888888888', '341@qq.com', '0', '0', 'admin', '2022-10-14 16:19:56', 'admin', '2022-10-20 09:48:44');
INSERT INTO `sys_dept` VALUES (105, 101, '0,100,101', '儿科', 3, '', '15888888888', '34@qq.com', '0', '0', 'admin', '2022-10-14 16:19:56', 'admin', '2022-10-20 09:49:01');
INSERT INTO `sys_dept` VALUES (106, 101, '0,100,101', '妇科', 4, ' ', '15888888888', '123@qq.com', '0', '0', 'admin', '2022-10-14 16:19:56', 'admin', '2022-10-20 09:49:17');
INSERT INTO `sys_dept` VALUES (108, 102, '0,100,102', '儿科', 1, '', '15888888888', '345@qq.com', '0', '0', 'admin', '2022-10-14 16:19:56', 'admin', '2022-10-20 09:49:52');
INSERT INTO `sys_dept` VALUES (109, 102, '0,100,102', '精神科', 2, '', '15888888888', '134@qq.com', '0', '0', 'admin', '2022-10-14 16:19:56', 'admin', '2022-10-20 09:50:19');

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY USING BTREE (`dict_code`)
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典数据表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '新增操作');
INSERT INTO `sys_dict_data` VALUES (19, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '修改操作');
INSERT INTO `sys_dict_data` VALUES (20, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '删除操作');
INSERT INTO `sys_dict_data` VALUES (21, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '授权操作');
INSERT INTO `sys_dict_data` VALUES (22, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '导出操作');
INSERT INTO `sys_dict_data` VALUES (23, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '导入操作');
INSERT INTO `sys_dict_data` VALUES (24, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '强退操作');
INSERT INTO `sys_dict_data` VALUES (25, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '生成操作');
INSERT INTO `sys_dict_data` VALUES (26, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '清空操作');
INSERT INTO `sys_dict_data` VALUES (27, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (28, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '停用状态');

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY USING BTREE (`dict_id`),
  UNIQUE INDEX `dict_type` USING BTREE(`dict_type`)
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典类型表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '登录状态列表');

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY USING BTREE (`job_id`, `job_name`, `job_group`)
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务调度表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2022-10-14 16:19:56', '', NULL, '');
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2022-10-14 16:19:56', '', NULL, '');
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2022-10-14 16:19:56', '', NULL, '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '日志信息',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '异常信息',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY USING BTREE (`job_log_id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务调度日志表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '提示消息',
  `login_time` datetime NULL DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY USING BTREE (`info_id`)
) ENGINE = InnoDB AUTO_INCREMENT = 135 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统访问记录' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES (100, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2022-10-14 22:28:23');
INSERT INTO `sys_logininfor` VALUES (101, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2022-10-15 20:43:05');
INSERT INTO `sys_logininfor` VALUES (102, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2022-10-15 20:43:09');
INSERT INTO `sys_logininfor` VALUES (103, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2022-10-15 21:48:00');
INSERT INTO `sys_logininfor` VALUES (104, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '1', '验证码错误', '2022-10-15 21:48:05');
INSERT INTO `sys_logininfor` VALUES (105, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2022-10-15 21:48:09');
INSERT INTO `sys_logininfor` VALUES (106, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-16 09:52:34');
INSERT INTO `sys_logininfor` VALUES (107, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2022-10-16 09:53:34');
INSERT INTO `sys_logininfor` VALUES (108, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '验证码错误', '2022-10-16 09:56:34');
INSERT INTO `sys_logininfor` VALUES (109, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-16 09:56:37');
INSERT INTO `sys_logininfor` VALUES (110, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2022-10-16 12:03:01');
INSERT INTO `sys_logininfor` VALUES (111, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-16 16:37:00');
INSERT INTO `sys_logininfor` VALUES (112, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-10-16 16:41:13');
INSERT INTO `sys_logininfor` VALUES (113, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-16 16:41:21');
INSERT INTO `sys_logininfor` VALUES (114, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2022-10-16 16:43:02');
INSERT INTO `sys_logininfor` VALUES (115, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '退出成功', '2022-10-16 16:43:11');
INSERT INTO `sys_logininfor` VALUES (116, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2022-10-16 16:43:21');
INSERT INTO `sys_logininfor` VALUES (117, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-16 16:44:08');
INSERT INTO `sys_logininfor` VALUES (118, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-10-16 16:44:18');
INSERT INTO `sys_logininfor` VALUES (119, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-16 16:44:41');
INSERT INTO `sys_logininfor` VALUES (120, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-10-16 18:59:28');
INSERT INTO `sys_logininfor` VALUES (121, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '验证码已失效', '2022-10-16 19:13:41');
INSERT INTO `sys_logininfor` VALUES (122, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-16 19:13:45');
INSERT INTO `sys_logininfor` VALUES (123, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-10-16 19:25:04');
INSERT INTO `sys_logininfor` VALUES (124, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-16 19:25:33');
INSERT INTO `sys_logininfor` VALUES (125, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2022-10-18 16:05:01');
INSERT INTO `sys_logininfor` VALUES (126, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-18 20:59:15');
INSERT INTO `sys_logininfor` VALUES (127, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-10-19 09:48:44');
INSERT INTO `sys_logininfor` VALUES (128, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-19 09:48:54');
INSERT INTO `sys_logininfor` VALUES (129, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2022-10-19 10:24:39');
INSERT INTO `sys_logininfor` VALUES (130, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '验证码错误', '2022-10-19 10:48:04');
INSERT INTO `sys_logininfor` VALUES (131, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-19 10:48:08');
INSERT INTO `sys_logininfor` VALUES (132, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-10-19 10:52:30');
INSERT INTO `sys_logininfor` VALUES (133, 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-19 10:52:35');
INSERT INTO `sys_logininfor` VALUES (134, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2022-10-20 10:18:26');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) NULL DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int(4) NULL DEFAULT 0 COMMENT '显示顺序',
  `path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组件路径',
  `query` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '路由参数',
  `is_frame` int(1) NULL DEFAULT 1 COMMENT '是否为外链（0是 1否）',
  `is_cache` int(1) NULL DEFAULT 0 COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY USING BTREE (`menu_id`)
) ENGINE = InnoDB AUTO_INCREMENT = 2055 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '菜单权限表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 1, 'system', NULL, '', 1, 0, 'M', '0', '0', '', 'system', 'admin', '2022-10-18 20:58:54', '', NULL, '系统管理目录');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 2, 'user', 'system/user/index', '', 1, 0, 'C', '0', '0', 'system:user:list', 'user', 'admin', '2022-10-18 20:58:54', 'admin', '2022-10-19 20:28:27', '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 3, 'role', 'system/role/index', '', 1, 0, 'C', '0', '0', 'system:role:list', 'peoples', 'admin', '2022-10-18 20:58:54', 'admin', '2022-10-19 20:30:59', '角色管理菜单');
INSERT INTO `sys_menu` VALUES (103, '科室管理', 1, 1, 'dept', 'system/dept/index', '', 1, 0, 'C', '0', '0', 'system:dept:list', 'tree', 'admin', '2022-10-18 20:58:54', 'admin', '2022-10-20 09:54:38', '部门管理菜单');
INSERT INTO `sys_menu` VALUES (107, '通知公告管理', 1, 4, 'notice', 'system/notice/index', '', 1, 0, 'C', '0', '0', 'system:notice:list', 'message', 'admin', '2022-10-18 20:58:54', 'admin', '2022-10-19 20:37:20', '通知公告菜单');
INSERT INTO `sys_menu` VALUES (1001, '用户查询', 100, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:user:query', '#', 'admin', '2022-10-18 20:58:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1002, '用户新增', 100, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:user:add', '#', 'admin', '2022-10-18 20:58:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1003, '用户修改', 100, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:user:edit', '#', 'admin', '2022-10-18 20:58:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1004, '用户删除', 100, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:user:remove', '#', 'admin', '2022-10-18 20:58:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1005, '用户导出', 100, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:user:export', '#', 'admin', '2022-10-18 20:58:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1006, '用户导入', 100, 6, '', '', '', 1, 0, 'F', '0', '0', 'system:user:import', '#', 'admin', '2022-10-18 20:58:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1007, '重置密码', 100, 7, '', '', '', 1, 0, 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', '2022-10-18 20:58:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1008, '角色查询', 101, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:role:query', '#', 'admin', '2022-10-18 20:58:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1009, '角色新增', 101, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:role:add', '#', 'admin', '2022-10-18 20:58:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1010, '角色修改', 101, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:role:edit', '#', 'admin', '2022-10-18 20:58:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1011, '角色删除', 101, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:role:remove', '#', 'admin', '2022-10-18 20:58:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1012, '角色导出', 101, 5, '', '', '', 1, 0, 'F', '0', '0', 'system:role:export', '#', 'admin', '2022-10-18 20:58:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1013, '菜单查询', 102, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:query', '#', 'admin', '2022-10-18 20:58:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1014, '菜单新增', 102, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:add', '#', 'admin', '2022-10-18 20:58:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1015, '菜单修改', 102, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:edit', '#', 'admin', '2022-10-18 20:58:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1016, '菜单删除', 102, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:menu:remove', '#', 'admin', '2022-10-18 20:58:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1017, '部门查询', 103, 1, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:query', '#', 'admin', '2022-10-18 20:58:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1018, '部门新增', 103, 2, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:add', '#', 'admin', '2022-10-18 20:58:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1019, '部门修改', 103, 3, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:edit', '#', 'admin', '2022-10-18 20:58:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1020, '部门删除', 103, 4, '', '', '', 1, 0, 'F', '0', '0', 'system:dept:remove', '#', 'admin', '2022-10-18 20:58:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1036, '公告查询', 107, 1, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:query', '#', 'admin', '2022-10-18 20:58:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1037, '公告新增', 107, 2, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:add', '#', 'admin', '2022-10-18 20:58:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1038, '公告修改', 107, 3, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:edit', '#', 'admin', '2022-10-18 20:58:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1039, '公告删除', 107, 4, '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:remove', '#', 'admin', '2022-10-18 20:58:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2004, '药品进销存管理', 0, 2, 'drugManage', NULL, NULL, 1, 0, 'M', '0', '0', '', 'clipboard', 'admin', '2022-10-15 20:19:14', 'admin', '2022-10-19 20:53:36', '');
INSERT INTO `sys_menu` VALUES (2005, '生产厂家维护', 2004, 1, 'factory', 'drugManage/factory/index', NULL, 1, 0, 'C', '0', '0', '', 'list', 'admin', '2022-10-15 20:20:47', 'admin', '2022-10-19 20:55:31', '');
INSERT INTO `sys_menu` VALUES (2024, '药品信息维护', 2004, 2, 'drug', 'drugManage/drug/index', NULL, 1, 0, 'C', '0', '0', '', 'list', 'admin', '2022-10-16 11:55:47', 'admin', '2022-10-19 20:55:42', '');
INSERT INTO `sys_menu` VALUES (2026, '检查费用设置', 1, 6, 'exam', 'system/exam/index', NULL, 1, 0, 'C', '0', '0', '', 'list', 'admin', '2022-10-19 20:40:29', 'admin', '2022-10-19 20:42:35', '');
INSERT INTO `sys_menu` VALUES (2027, '挂号费用设置', 1, 7, 'regis', 'system/regis/index', NULL, 1, 0, 'C', '0', '0', NULL, 'list', 'admin', '2022-10-19 20:45:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2028, '供应商维护', 2004, 3, 'provider', 'drugManage/provider/index', NULL, 1, 0, 'C', '0', '0', '', 'list', 'admin', '2022-10-19 20:56:40', 'admin', '2022-10-19 20:57:01', '');
INSERT INTO `sys_menu` VALUES (2029, '采购入库', 2004, 4, 'purchase', 'drugManage/purchase/index', NULL, 1, 0, 'C', '0', '0', NULL, 'list', 'admin', '2022-10-19 20:58:12', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2030, '入库审核', 2004, 5, 'examine', 'drugManage/examine/index', NULL, 1, 0, 'C', '0', '0', NULL, 'list', 'admin', '2022-10-19 20:59:16', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2031, '库存查询', 2004, 6, 'inventory', 'drugManage/inventory/index', NULL, 1, 0, 'C', '0', '0', NULL, 'list', 'admin', '2022-10-19 21:00:35', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2032, '看病就诊', 0, 3, 'doctor', NULL, NULL, 1, 0, 'M', '0', '0', NULL, 'excel', 'admin', '2022-10-19 21:02:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2033, '门诊挂号', 2032, 1, 'register', 'doctor/register/index', NULL, 1, 0, 'C', '0', '0', NULL, 'list', 'admin', '2022-10-19 21:04:05', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2034, '挂号列表', 2032, 3, 'regislist', 'doctor/regislist/index', NULL, 1, 0, 'C', '0', '0', NULL, 'list', 'admin', '2022-10-19 21:06:47', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2035, '新开就诊', 2032, 3, 'newcare', 'doctor/newcare/index', NULL, 1, 0, 'C', '0', '0', NULL, 'list', 'admin', '2022-10-19 21:08:29', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2036, '我的排班', 2032, 4, 'mysched', 'doctor/mysched/index', NULL, 1, 0, 'C', '0', '0', NULL, 'list', 'admin', '2022-10-19 21:09:53', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2037, '医生排班', 2032, 5, 'sched', 'doctor/sched/index', NULL, 1, 0, 'C', '0', '0', '', 'list', 'admin', '2022-10-19 21:10:57', 'admin', '2022-10-19 21:12:19', '');
INSERT INTO `sys_menu` VALUES (2038, '患者库', 2032, 6, 'patient', 'doctor/patient/index', NULL, 1, 0, 'C', '0', '0', NULL, 'list', 'admin', '2022-10-19 21:12:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2039, '收费管理', 0, 4, 'charge', NULL, NULL, 1, 0, 'M', '0', '0', NULL, 'excel', 'admin', '2022-10-19 21:17:54', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2041, '处方收费', 2039, 1, 'charge', 'charge/charge/index', NULL, 1, 0, 'C', '0', '0', '', 'list', 'admin', '2022-10-19 21:22:41', 'admin', '2022-10-19 21:23:46', '');
INSERT INTO `sys_menu` VALUES (2042, '收费查询', 2039, 2, 'chargelist', 'charge/chargelist/index', NULL, 1, 0, 'C', '0', '0', '', 'list', 'admin', '2022-10-19 21:23:36', 'admin', '2022-10-19 21:23:54', '');
INSERT INTO `sys_menu` VALUES (2043, '处方退费', 2039, 3, 'backfee', 'charge/backfee/index', NULL, 1, 0, 'C', '0', '0', NULL, 'list', 'admin', '2022-10-19 21:25:49', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2044, '退费查询', 2039, 4, 'backfeelist', 'charge/backfeelist/index', NULL, 1, 0, 'C', '0', '0', NULL, 'list', 'admin', '2022-10-19 21:29:58', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2045, '处方发药', 2039, 5, 'dispense', 'charge/dispense/index', NULL, 1, 0, 'C', '0', '0', NULL, 'list', 'admin', '2022-10-19 21:35:13', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2046, '检查管理', 0, 5, 'check', NULL, NULL, 1, 0, 'M', '0', '0', NULL, 'tab', 'admin', '2022-10-19 21:39:37', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2047, '新开检查', 2046, 1, 'docheck', 'check/docheck/index', NULL, 1, 0, 'C', '0', '0', NULL, 'list', 'admin', '2022-10-19 21:42:27', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2048, '检查结果录入', 2046, 2, 'checkresult', 'check/checkresult/index', NULL, 1, 0, 'C', '0', '0', NULL, 'list', 'admin', '2022-10-19 21:43:32', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2049, '检查结果查询', 2046, 3, 'checklist', 'check/checklist/index', NULL, 1, 0, 'C', '0', '0', NULL, 'list', 'admin', '2022-10-19 21:44:52', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2050, '数据统计', 0, 6, 'statis', NULL, NULL, 1, 0, 'M', '0', '0', NULL, 'druid', 'admin', '2022-10-19 21:49:15', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2051, '收支统计', 2050, 1, 'revenue', 'statis/revenue/index', NULL, 1, 0, 'C', '0', '0', NULL, 'list', 'admin', '2022-10-19 21:50:05', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2052, '药品销售统计', 2050, 2, 'sales', 'statis/sales/index', NULL, 1, 0, 'C', '0', '0', NULL, 'list', 'admin', '2022-10-19 21:51:00', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2053, '检查项目统计', 2050, 3, 'check', 'statis/check/index', NULL, 1, 0, 'C', '0', '0', NULL, 'list', 'admin', '2022-10-19 21:52:59', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2054, '工作量统计', 2050, 4, 'workload', 'statis/workload/index', NULL, 1, 0, 'C', '0', '0', NULL, 'list', 'admin', '2022-10-19 21:53:48', '', NULL, '');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公告标题',
  `notice_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` longblob NULL COMMENT '公告内容',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY USING BTREE (`notice_id`)
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '通知公告表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (1, '温馨提醒：新版本发布啦', '2', 0x3C703EE696B0E78988E69CACE58685E5AEB93C2F703E, '0', 'admin', '2022-10-14 16:19:56', 'admin', '2022-10-19 20:53:02', '管理员');
INSERT INTO `sys_notice` VALUES (2, '维护通知：系统凌晨维护', '1', 0x3C703EE7BBB4E68AA4E58685E5AEB93C2F703E, '0', 'admin', '2022-10-14 16:19:56', 'admin', '2022-10-19 20:53:11', '管理员');

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) NULL DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) NULL DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '返回参数',
  `status` int(1) NULL DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY USING BTREE (`oper_id`)
) ENGINE = InnoDB AUTO_INCREMENT = 381 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '操作日志记录' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES (100, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"button\",\"orderNum\":\"1\",\"menuName\":\"搜索\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"单独\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-15 20:17:59');
INSERT INTO `sys_oper_log` VALUES (101, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"button\",\"orderNum\":\"1\",\"menuName\":\"搜索\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"单独\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-15 20:17:59');
INSERT INTO `sys_oper_log` VALUES (102, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"button\",\"orderNum\":\"1\",\"menuName\":\"搜索\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"单独\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"新增菜单\'搜索\'失败，菜单名称已存在\",\"code\":500}', 0, NULL, '2022-10-15 20:17:59');
INSERT INTO `sys_oper_log` VALUES (103, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"button\",\"orderNum\":\"1\",\"menuName\":\"搜索\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"单独\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-15 20:17:59');
INSERT INTO `sys_oper_log` VALUES (104, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"button\",\"orderNum\":\"1\",\"menuName\":\"搜索\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"单独\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"新增菜单\'搜索\'失败，菜单名称已存在\",\"code\":500}', 0, NULL, '2022-10-15 20:17:59');
INSERT INTO `sys_oper_log` VALUES (105, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"button\",\"orderNum\":\"1\",\"menuName\":\"搜索\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"单独\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"新增菜单\'搜索\'失败，菜单名称已存在\",\"code\":500}', 0, NULL, '2022-10-15 20:17:59');
INSERT INTO `sys_oper_log` VALUES (106, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"button\",\"orderNum\":\"1\",\"menuName\":\"搜索\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"单独\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"新增菜单\'搜索\'失败，菜单名称已存在\",\"code\":500}', 0, NULL, '2022-10-15 20:17:59');
INSERT INTO `sys_oper_log` VALUES (107, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"button\",\"orderNum\":\"1\",\"menuName\":\"搜索\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"单独\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-15 20:17:59');
INSERT INTO `sys_oper_log` VALUES (108, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"button\",\"orderNum\":\"1\",\"menuName\":\"搜索\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"单独\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"新增菜单\'搜索\'失败，菜单名称已存在\",\"code\":500}', 0, NULL, '2022-10-15 20:17:59');
INSERT INTO `sys_oper_log` VALUES (109, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"button\",\"orderNum\":\"1\",\"menuName\":\"搜索\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"单独\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"新增菜单\'搜索\'失败，菜单名称已存在\",\"code\":500}', 0, NULL, '2022-10-15 20:17:59');
INSERT INTO `sys_oper_log` VALUES (110, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/2000', '127.0.0.1', '内网IP', '{menuId=2000}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-15 20:18:20');
INSERT INTO `sys_oper_log` VALUES (111, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/2001', '127.0.0.1', '内网IP', '{menuId=2001}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-15 20:18:23');
INSERT INTO `sys_oper_log` VALUES (112, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/2002', '127.0.0.1', '内网IP', '{menuId=2002}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-15 20:18:25');
INSERT INTO `sys_oper_log` VALUES (113, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/2003', '127.0.0.1', '内网IP', '{menuId=2003}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-15 20:18:28');
INSERT INTO `sys_oper_log` VALUES (114, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"clipboard\",\"orderNum\":\"6\",\"menuName\":\"药品进销存管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"https://drugManage\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-15 20:19:14');
INSERT INTO `sys_oper_log` VALUES (115, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"build\",\"orderNum\":\"1\",\"menuName\":\"生产厂家维护\",\"params\":{},\"parentId\":2004,\"isCache\":\"0\",\"path\":\"https://factory\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-15 20:20:47');
INSERT INTO `sys_oper_log` VALUES (116, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"clipboard\",\"orderNum\":\"6\",\"menuName\":\"药品进销存管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"drugManage\",\"children\":[],\"createTime\":1665836354000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2004,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-15 20:43:32');
INSERT INTO `sys_oper_log` VALUES (117, '代码生成', 6, 'com.ruoyi.project.tool.gen.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'factory_info', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-15 21:06:11');
INSERT INTO `sys_oper_log` VALUES (118, '代码生成', 8, 'com.ruoyi.project.tool.gen.controller.GenController.batchGenCode()', 'GET', 1, 'admin', NULL, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', NULL, 0, NULL, '2022-10-15 21:07:22');
INSERT INTO `sys_oper_log` VALUES (119, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"build\",\"orderNum\":\"1\",\"menuName\":\"生产厂家维护\",\"params\":{},\"parentId\":2004,\"isCache\":\"0\",\"path\":\"https://factory/index.vue\",\"children\":[],\"createTime\":1665836447000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2005,\"menuType\":\"C\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-15 21:38:31');
INSERT INTO `sys_oper_log` VALUES (120, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"build\",\"orderNum\":\"1\",\"menuName\":\"生产厂家维护\",\"params\":{},\"parentId\":2004,\"isCache\":\"0\",\"path\":\"https://factory\",\"children\":[],\"createTime\":1665836447000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2005,\"menuType\":\"C\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-15 21:39:18');
INSERT INTO `sys_oper_log` VALUES (121, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"build\",\"orderNum\":\"1\",\"menuName\":\"生产厂家维护\",\"params\":{},\"parentId\":2004,\"isCache\":\"0\",\"path\":\"factory\",\"children\":[],\"createTime\":1665836447000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2005,\"menuType\":\"C\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-15 21:49:44');
INSERT INTO `sys_oper_log` VALUES (122, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"build\",\"orderNum\":\"1\",\"menuName\":\"生产厂家维护\",\"params\":{},\"parentId\":2004,\"isCache\":\"0\",\"path\":\"factory\",\"component\":\"drugManage/factory\",\"children\":[],\"createTime\":1665836447000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2005,\"menuType\":\"C\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-15 21:50:24');
INSERT INTO `sys_oper_log` VALUES (123, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"build\",\"orderNum\":\"1\",\"menuName\":\"生产厂家维护\",\"params\":{},\"parentId\":2004,\"isCache\":\"0\",\"path\":\"factory\",\"component\":\"drugManage/factory/index\",\"children\":[],\"createTime\":1665836447000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2005,\"menuType\":\"C\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-15 21:53:16');
INSERT INTO `sys_oper_log` VALUES (124, 'InnoDB free: 11264 kB', 1, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.add()', 'POST', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"1\",\"factoryName\":\"1\",\"params\":{},\"createTime\":1665843571824,\"contact\":\"1\",\"tel\":\"1\",\"status\":\"0\"}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n### The error may exist in file [D:\\Project\\Practice\\NoRedis\\Server-fast-master\\target\\classes\\mybatis\\drugManage\\FactoryInfoMapper.xml]\r\n### The error may involve com.ruoyi.project.drugManage.mapper.FactoryInfoMapper.insertFactoryInfo-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into factory_info          ( factory_name,             factory_code,             contact,             tel,                          status,             create_time )           values ( ?,             ?,             ?,             ?,                          ?,             ? )\r\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value', '2022-10-15 22:19:31');
INSERT INTO `sys_oper_log` VALUES (125, 'InnoDB free: 11264 kB', 1, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.add()', 'POST', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"1\",\"factoryName\":\"2\",\"params\":{},\"createTime\":1665843577385,\"contact\":\"1\",\"tel\":\"1\",\"status\":\"0\"}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n### The error may exist in file [D:\\Project\\Practice\\NoRedis\\Server-fast-master\\target\\classes\\mybatis\\drugManage\\FactoryInfoMapper.xml]\r\n### The error may involve com.ruoyi.project.drugManage.mapper.FactoryInfoMapper.insertFactoryInfo-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into factory_info          ( factory_name,             factory_code,             contact,             tel,                          status,             create_time )           values ( ?,             ?,             ?,             ?,                          ?,             ? )\r\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value', '2022-10-15 22:19:37');
INSERT INTO `sys_oper_log` VALUES (126, 'InnoDB free: 11264 kB', 1, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.add()', 'POST', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"2\",\"factoryName\":\"2\",\"params\":{},\"createTime\":1665843581734,\"contact\":\"2\",\"tel\":\"2\",\"status\":\"0\"}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n### The error may exist in file [D:\\Project\\Practice\\NoRedis\\Server-fast-master\\target\\classes\\mybatis\\drugManage\\FactoryInfoMapper.xml]\r\n### The error may involve com.ruoyi.project.drugManage.mapper.FactoryInfoMapper.insertFactoryInfo-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into factory_info          ( factory_name,             factory_code,             contact,             tel,                          status,             create_time )           values ( ?,             ?,             ?,             ?,                          ?,             ? )\r\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value', '2022-10-15 22:19:41');
INSERT INTO `sys_oper_log` VALUES (127, 'InnoDB free: 11264 kB', 1, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.add()', 'POST', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryName\":\"2\",\"params\":{},\"createTime\":1665843935079,\"status\":\"0\"}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n### The error may exist in file [D:\\Project\\Practice\\NoRedis\\Server-fast-master\\target\\classes\\mybatis\\drugManage\\FactoryInfoMapper.xml]\r\n### The error may involve com.ruoyi.project.drugManage.mapper.FactoryInfoMapper.insertFactoryInfo-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into factory_info          ( factory_name,                                                                 status,             create_time )           values ( ?,                                                                 ?,             ? )\r\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value', '2022-10-15 22:25:35');
INSERT INTO `sys_oper_log` VALUES (128, '代码生成', 3, 'com.ruoyi.project.tool.gen.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/tool/gen/1', '127.0.0.1', '内网IP', '{tableIds=1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-15 22:36:52');
INSERT INTO `sys_oper_log` VALUES (129, '代码生成', 6, 'com.ruoyi.project.tool.gen.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'factory_info', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-15 22:45:16');
INSERT INTO `sys_oper_log` VALUES (130, '代码生成', 6, 'com.ruoyi.project.tool.gen.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'sys_dict_type', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-15 22:45:47');
INSERT INTO `sys_oper_log` VALUES (131, '代码生成', 3, 'com.ruoyi.project.tool.gen.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/tool/gen/2', '127.0.0.1', '内网IP', '{tableIds=2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-15 22:50:43');
INSERT INTO `sys_oper_log` VALUES (132, '代码生成', 3, 'com.ruoyi.project.tool.gen.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/tool/gen/3', '127.0.0.1', '内网IP', '{tableIds=3}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-15 22:50:45');
INSERT INTO `sys_oper_log` VALUES (133, '代码生成', 6, 'com.ruoyi.project.tool.gen.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'factory_info', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-15 22:50:54');
INSERT INTO `sys_oper_log` VALUES (134, 'InnoDB free: 11264 kB', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"2\",\"factoryName\":\"1\",\"updateTime\":1665848480580,\"params\":{},\"contact\":\"1\",\"tel\":\"1\",\"id\":\"1\",\"keyword\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-15 23:41:20');
INSERT INTO `sys_oper_log` VALUES (135, 'InnoDB free: 11264 kB', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"2\",\"isDelete\":\"1\",\"factoryName\":\"1\",\"updateTime\":1665848490304,\"params\":{},\"contact\":\"1\",\"tel\":\"1\",\"id\":\"1\",\"keyword\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-15 23:41:30');
INSERT INTO `sys_oper_log` VALUES (136, 'InnoDB free: 11264 kB', 1, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.add()', 'POST', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"1\",\"factoryName\":\"我\",\"params\":{},\"createTime\":1665848520636,\"status\":\"0\"}', NULL, 1, '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n### The error may exist in file [D:\\Project\\Practice\\NoRedis\\Server-fast-master\\target\\classes\\mybatis\\drugManage\\FactoryInfoMapper.xml]\r\n### The error may involve com.ruoyi.project.drugManage.mapper.FactoryInfoMapper.insertFactoryInfo-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into factory_info          ( factory_name,             factory_code,                                                    status,             create_time )           values ( ?,             ?,                                                    ?,             ? )\r\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value', '2022-10-15 23:42:00');
INSERT INTO `sys_oper_log` VALUES (137, 'InnoDB free: 11264 kB', 1, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.add()', 'POST', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"1\",\"factoryName\":\"我\",\"params\":{},\"createTime\":1665848884272,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-15 23:48:04');
INSERT INTO `sys_oper_log` VALUES (138, 'InnoDB free: 11264 kB', 1, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.add()', 'POST', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"1\",\"factoryName\":\"2\",\"params\":{},\"createTime\":1665848896335,\"contact\":\"二\",\"tel\":\"手房\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-15 23:48:16');
INSERT INTO `sys_oper_log` VALUES (139, 'InnoDB free: 11264 kB', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"1\",\"factoryName\":\"2\",\"updateTime\":1665850287183,\"params\":{},\"createTime\":1665848896000,\"contact\":\"二\",\"tel\":\"手房\",\"id\":\"2\",\"keyword\":\"下次\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-16 00:11:27');
INSERT INTO `sys_oper_log` VALUES (140, 'InnoDB free: 11264 kB', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"1\",\"factoryName\":\"我\",\"updateTime\":1665850295492,\"params\":{},\"createTime\":1665848884000,\"id\":\"1\",\"keyword\":\"许\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-16 00:11:35');
INSERT INTO `sys_oper_log` VALUES (141, '角色管理', 2, 'com.ruoyi.project.system.controller.SysRoleController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/role/changeStatus', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":1,\"admin\":true,\"params\":{},\"deptCheckStrictly\":false,\"menuCheckStrictly\":false,\"status\":\"1\"}', NULL, 1, '不允许操作超级管理员角色', '2022-10-16 10:18:31');
INSERT INTO `sys_oper_log` VALUES (142, '角色管理', 2, 'com.ruoyi.project.system.controller.SysRoleController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/role/changeStatus', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":2,\"admin\":false,\"params\":{},\"deptCheckStrictly\":false,\"updateBy\":\"admin\",\"menuCheckStrictly\":false,\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-16 10:18:34');
INSERT INTO `sys_oper_log` VALUES (143, '角色管理', 2, 'com.ruoyi.project.system.controller.SysRoleController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/role/changeStatus', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":2,\"admin\":false,\"params\":{},\"deptCheckStrictly\":false,\"updateBy\":\"admin\",\"menuCheckStrictly\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-16 10:18:45');
INSERT INTO `sys_oper_log` VALUES (144, 'InnoDB free: 11264 kB', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"1\",\"factoryName\":\"我\",\"updateTime\":1665888686981,\"params\":{},\"createTime\":1665848884000,\"id\":\"1\",\"keyword\":\"许\",\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-16 10:51:27');
INSERT INTO `sys_oper_log` VALUES (145, 'InnoDB free: 11264 kB', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"1\",\"factoryName\":\"我\",\"remark\":\"安徽省\",\"updateTime\":1665889337878,\"params\":{},\"createTime\":1665848884000,\"id\":\"1\",\"keyword\":\"许\",\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-16 11:02:17');
INSERT INTO `sys_oper_log` VALUES (146, 'InnoDB free: 11264 kB', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"1\",\"factoryName\":\"2\",\"remark\":\"合肥市\",\"updateTime\":1665889355109,\"params\":{},\"createTime\":1665848896000,\"contact\":\"二\",\"tel\":\"手房\",\"id\":\"2\",\"keyword\":\"下次\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-16 11:02:35');
INSERT INTO `sys_oper_log` VALUES (147, 'InnoDB free: 11264 kB', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"1\",\"factoryName\":\"我\",\"remark\":\"安徽省\",\"updateTime\":1665889391882,\"params\":{},\"createTime\":1665848884000,\"tel\":\"1\",\"id\":\"1\",\"keyword\":\"许\",\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-16 11:03:11');
INSERT INTO `sys_oper_log` VALUES (148, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/4', '127.0.0.1', '内网IP', '{menuId=4}', '{\"msg\":\"菜单已分配,不允许删除\",\"code\":500}', 0, NULL, '2022-10-16 11:11:35');
INSERT INTO `sys_oper_log` VALUES (149, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/4', '127.0.0.1', '内网IP', '{menuId=4}', '{\"msg\":\"菜单已分配,不允许删除\",\"code\":500}', 0, NULL, '2022-10-16 11:12:07');
INSERT INTO `sys_oper_log` VALUES (150, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/3', '127.0.0.1', '内网IP', '{menuId=3}', '{\"msg\":\"存在子菜单,不允许删除\",\"code\":500}', 0, NULL, '2022-10-16 11:14:14');
INSERT INTO `sys_oper_log` VALUES (151, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/103', '127.0.0.1', '内网IP', '{menuId=103}', '{\"msg\":\"存在子菜单,不允许删除\",\"code\":500}', 0, NULL, '2022-10-16 11:14:38');
INSERT INTO `sys_oper_log` VALUES (152, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1017', '127.0.0.1', '内网IP', '{menuId=1017}', '{\"msg\":\"菜单已分配,不允许删除\",\"code\":500}', 0, NULL, '2022-10-16 11:14:57');
INSERT INTO `sys_oper_log` VALUES (153, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1', '127.0.0.1', '内网IP', '{menuId=1}', '{\"msg\":\"存在子菜单,不允许删除\",\"code\":500}', 0, NULL, '2022-10-16 11:15:29');
INSERT INTO `sys_oper_log` VALUES (154, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1026', '127.0.0.1', '内网IP', '{menuId=1026}', '{\"msg\":\"菜单已分配,不允许删除\",\"code\":500}', 0, NULL, '2022-10-16 11:15:38');
INSERT INTO `sys_oper_log` VALUES (155, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1', '127.0.0.1', '内网IP', '{menuId=1}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-16 11:16:58');
INSERT INTO `sys_oper_log` VALUES (156, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/2', '127.0.0.1', '内网IP', '{menuId=2}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-16 11:17:03');
INSERT INTO `sys_oper_log` VALUES (157, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/3', '127.0.0.1', '内网IP', '{menuId=3}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-16 11:17:18');
INSERT INTO `sys_oper_log` VALUES (158, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/4', '127.0.0.1', '内网IP', '{menuId=4}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-16 11:17:22');
INSERT INTO `sys_oper_log` VALUES (159, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/5', '127.0.0.1', '内网IP', '{menuId=5}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-16 11:17:28');
INSERT INTO `sys_oper_log` VALUES (160, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/100', '127.0.0.1', '内网IP', '{menuId=100}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-16 11:17:34');
INSERT INTO `sys_oper_log` VALUES (161, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/101', '127.0.0.1', '内网IP', '{menuId=101}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-16 11:17:37');
INSERT INTO `sys_oper_log` VALUES (162, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/102', '127.0.0.1', '内网IP', '{menuId=102}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-16 11:17:40');
INSERT INTO `sys_oper_log` VALUES (163, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/103', '127.0.0.1', '内网IP', '{menuId=103}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-16 11:17:43');
INSERT INTO `sys_oper_log` VALUES (164, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/104', '127.0.0.1', '内网IP', '{menuId=104}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-16 11:17:46');
INSERT INTO `sys_oper_log` VALUES (165, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/105', '127.0.0.1', '内网IP', '{menuId=105}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-16 11:17:49');
INSERT INTO `sys_oper_log` VALUES (166, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/106', '127.0.0.1', '内网IP', '{menuId=106}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-16 11:17:59');
INSERT INTO `sys_oper_log` VALUES (167, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/107', '127.0.0.1', '内网IP', '{menuId=107}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-16 11:18:02');
INSERT INTO `sys_oper_log` VALUES (168, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/108', '127.0.0.1', '内网IP', '{menuId=108}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-16 11:18:05');
INSERT INTO `sys_oper_log` VALUES (169, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/500', '127.0.0.1', '内网IP', '{menuId=500}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-16 11:18:10');
INSERT INTO `sys_oper_log` VALUES (170, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1042', '127.0.0.1', '内网IP', '{menuId=1042}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-16 11:18:14');
INSERT INTO `sys_oper_log` VALUES (171, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1041', '127.0.0.1', '内网IP', '{menuId=1041}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-16 11:18:17');
INSERT INTO `sys_oper_log` VALUES (172, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1040', '127.0.0.1', '内网IP', '{menuId=1040}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-16 11:18:22');
INSERT INTO `sys_oper_log` VALUES (173, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/501', '127.0.0.1', '内网IP', '{menuId=501}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-16 11:18:24');
INSERT INTO `sys_oper_log` VALUES (174, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"cascader\",\"orderNum\":\"2\",\"menuName\":\"drug\",\"params\":{},\"parentId\":2004,\"isCache\":\"0\",\"path\":\"drug\",\"component\":\"drugManage/drug/index\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-16 11:55:47');
INSERT INTO `sys_oper_log` VALUES (175, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"cascader\",\"orderNum\":\"2\",\"menuName\":\"药品信息维护\",\"params\":{},\"parentId\":2004,\"isCache\":\"0\",\"path\":\"drug\",\"component\":\"drugManage/drug/index\",\"children\":[],\"createTime\":1665892547000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2024,\"menuType\":\"C\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-16 11:56:39');
INSERT INTO `sys_oper_log` VALUES (176, 'InnoDB free: 11264 kB', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1665892717490,\"params\":{},\"createTime\":1665848884000,\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-16 11:58:37');
INSERT INTO `sys_oper_log` VALUES (177, 'InnoDB free: 11264 kB', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"112313\",\"factoryName\":\"上海医药（集团）有限公司\",\"remark\":\"上海市\",\"updateTime\":1665892818001,\"params\":{},\"createTime\":1665848896000,\"contact\":\"阿雷\",\"tel\":\"0551-23232323\",\"id\":\"2\",\"keyword\":\"下次\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-16 12:00:18');
INSERT INTO `sys_oper_log` VALUES (178, 'InnoDB free: 11264 kB', 1, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.add()', 'POST', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryName\":\"FVA\",\"remark\":\"EFD\",\"params\":{},\"createTime\":1665919572392,\"tel\":\"123123123123\",\"keyword\":\"SDF\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-16 19:26:12');
INSERT INTO `sys_oper_log` VALUES (179, 'InnoDB free: 11264 kB', 3, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.remove()', 'DELETE', 1, 'admin', NULL, '/factory/info/3', '127.0.0.1', '内网IP', '{ids=3}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-16 19:26:20');
INSERT INTO `sys_oper_log` VALUES (180, 'InnoDB free: 11264 kB', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1665919621584,\"params\":{},\"createTime\":1665848884000,\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-16 19:27:01');
INSERT INTO `sys_oper_log` VALUES (181, 'InnoDB free: 11264 kB', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1665924058139,\"params\":{},\"createTime\":1665848884000,\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-16 20:40:58');
INSERT INTO `sys_oper_log` VALUES (182, '代码生成', 3, 'com.ruoyi.project.tool.gen.controller.GenController.remove()', 'DELETE', 1, 'admin', NULL, '/tool/gen/4', '127.0.0.1', '内网IP', '{tableIds=4}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-18 20:59:50');
INSERT INTO `sys_oper_log` VALUES (183, '代码生成', 6, 'com.ruoyi.project.tool.gen.controller.GenController.importTableSave()', 'POST', 1, 'admin', NULL, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'factory_info', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-18 20:59:55');
INSERT INTO `sys_oper_log` VALUES (184, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666146450140,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"正常\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 10:27:30');
INSERT INTO `sys_oper_log` VALUES (185, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666146498645,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 10:28:18');
INSERT INTO `sys_oper_log` VALUES (186, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666146504273,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 10:28:24');
INSERT INTO `sys_oper_log` VALUES (187, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666146520015,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"正常\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 10:28:40');
INSERT INTO `sys_oper_log` VALUES (188, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666146524209,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"停用\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 10:28:44');
INSERT INTO `sys_oper_log` VALUES (189, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666146528601,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"停用\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 10:28:48');
INSERT INTO `sys_oper_log` VALUES (190, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666146533711,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"正常\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 10:28:53');
INSERT INTO `sys_oper_log` VALUES (191, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666146537695,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"停用\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 10:28:57');
INSERT INTO `sys_oper_log` VALUES (192, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666146591942,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"正常\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 10:29:51');
INSERT INTO `sys_oper_log` VALUES (193, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666146595659,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"停用\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 10:29:55');
INSERT INTO `sys_oper_log` VALUES (194, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666146604524,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"停用\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 10:30:04');
INSERT INTO `sys_oper_log` VALUES (195, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666146689382,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"正常\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 10:31:29');
INSERT INTO `sys_oper_log` VALUES (196, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666146693400,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"停用\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 10:31:33');
INSERT INTO `sys_oper_log` VALUES (197, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666146723898,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 10:32:03');
INSERT INTO `sys_oper_log` VALUES (198, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666146727633,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 10:32:07');
INSERT INTO `sys_oper_log` VALUES (199, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666146731977,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 10:32:12');
INSERT INTO `sys_oper_log` VALUES (200, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666146741003,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 10:32:21');
INSERT INTO `sys_oper_log` VALUES (201, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666146756716,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 10:32:36');
INSERT INTO `sys_oper_log` VALUES (202, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666146761677,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 10:32:41');
INSERT INTO `sys_oper_log` VALUES (203, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666146838102,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"正常\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 10:33:58');
INSERT INTO `sys_oper_log` VALUES (204, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666146841623,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"停用\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 10:34:01');
INSERT INTO `sys_oper_log` VALUES (205, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666146845715,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"正常\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 10:34:05');
INSERT INTO `sys_oper_log` VALUES (206, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666146849362,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"停用\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 10:34:09');
INSERT INTO `sys_oper_log` VALUES (207, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666147702548,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"正常\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 10:48:22');
INSERT INTO `sys_oper_log` VALUES (208, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666147706641,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"停用\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 10:48:26');
INSERT INTO `sys_oper_log` VALUES (209, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666147730163,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"正常\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 10:48:50');
INSERT INTO `sys_oper_log` VALUES (210, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666147735173,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"停用\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 10:48:55');
INSERT INTO `sys_oper_log` VALUES (211, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666147950155,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"停用\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 10:52:30');
INSERT INTO `sys_oper_log` VALUES (212, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666147950141,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"停用\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 10:52:30');
INSERT INTO `sys_oper_log` VALUES (213, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666147950155,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"停用\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 10:52:30');
INSERT INTO `sys_oper_log` VALUES (214, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666147950155,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"停用\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 10:52:30');
INSERT INTO `sys_oper_log` VALUES (215, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666148319307,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"停用\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 10:58:39');
INSERT INTO `sys_oper_log` VALUES (216, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666148323127,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"正常\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 10:58:43');
INSERT INTO `sys_oper_log` VALUES (217, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666148327145,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"停用\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 10:58:47');
INSERT INTO `sys_oper_log` VALUES (218, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666148331555,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"正常\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 10:58:51');
INSERT INTO `sys_oper_log` VALUES (219, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666148409747,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"停用\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 11:00:09');
INSERT INTO `sys_oper_log` VALUES (220, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666148550714,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"停用\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 11:02:30');
INSERT INTO `sys_oper_log` VALUES (221, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, NULL, NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666148556763,\"params\":{},\"createTime\":1665848884000,\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"正常\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 11:02:36');
INSERT INTO `sys_oper_log` VALUES (222, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666148736486,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"正常\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 11:05:36');
INSERT INTO `sys_oper_log` VALUES (223, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666148998906,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"正常\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 11:09:58');
INSERT INTO `sys_oper_log` VALUES (224, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666149017846,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"停用\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 11:10:17');
INSERT INTO `sys_oper_log` VALUES (225, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666149154881,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"正常\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 11:12:34');
INSERT INTO `sys_oper_log` VALUES (226, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, NULL, NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666149161365,\"params\":{},\"createTime\":1665848884000,\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"停用\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 11:12:41');
INSERT INTO `sys_oper_log` VALUES (227, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666149168184,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"正常\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 11:12:48');
INSERT INTO `sys_oper_log` VALUES (228, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666149173195,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"停用\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 11:12:53');
INSERT INTO `sys_oper_log` VALUES (229, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666149262134,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"正常\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 11:14:22');
INSERT INTO `sys_oper_log` VALUES (230, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666149266269,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"停用\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 11:14:26');
INSERT INTO `sys_oper_log` VALUES (231, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666149280711,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"正常\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 11:14:40');
INSERT INTO `sys_oper_log` VALUES (232, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666149667005,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"正常\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 11:21:07');
INSERT INTO `sys_oper_log` VALUES (233, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666149671176,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"停用\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 11:21:11');
INSERT INTO `sys_oper_log` VALUES (234, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666149849316,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 11:24:09');
INSERT INTO `sys_oper_log` VALUES (235, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666149853279,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 11:24:13');
INSERT INTO `sys_oper_log` VALUES (236, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666149864143,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 11:24:24');
INSERT INTO `sys_oper_log` VALUES (237, '修改生产厂家维护信息', 2, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.edit()', 'PUT', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"000538\",\"factoryName\":\"云南白药集团\",\"remark\":\"云南省\",\"updateTime\":1666149873344,\"params\":{},\"createTime\":1665848884000,\"updateBy\":\"admin\",\"contact\":\"ADC\",\"tel\":\"18099876656\",\"id\":\"1\",\"keyword\":\"yyds\",\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 11:24:33');
INSERT INTO `sys_oper_log` VALUES (238, '角色管理', 2, 'com.ruoyi.project.system.controller.SysRoleController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/role/changeStatus', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":2,\"admin\":false,\"params\":{},\"deptCheckStrictly\":false,\"updateBy\":\"admin\",\"menuCheckStrictly\":false,\"status\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 18:12:59');
INSERT INTO `sys_oper_log` VALUES (239, '角色管理', 2, 'com.ruoyi.project.system.controller.SysRoleController.changeStatus()', 'PUT', 1, 'admin', NULL, '/system/role/changeStatus', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":2,\"admin\":false,\"params\":{},\"deptCheckStrictly\":false,\"updateBy\":\"admin\",\"menuCheckStrictly\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 18:13:03');
INSERT INTO `sys_oper_log` VALUES (240, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"tree\",\"orderNum\":\"1\",\"menuName\":\"科室管理\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"deptMan\",\"component\":\"system/deptMan\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:28:15');
INSERT INTO `sys_oper_log` VALUES (241, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"user\",\"orderNum\":\"2\",\"menuName\":\"用户管理\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"user\",\"component\":\"system/user/index\",\"children\":[],\"createTime\":1666097934000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":100,\"menuType\":\"C\",\"perms\":\"system:user:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:28:27');
INSERT INTO `sys_oper_log` VALUES (242, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"tree\",\"orderNum\":\"1\",\"menuName\":\"科室管理\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"deptMan\",\"component\":\"system/deptMan/index\",\"children\":[],\"createTime\":1666182495000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2025,\"menuType\":\"C\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:30:26');
INSERT INTO `sys_oper_log` VALUES (243, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"peoples\",\"orderNum\":\"3\",\"menuName\":\"角色管理\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"role\",\"component\":\"system/role/index\",\"children\":[],\"createTime\":1666097934000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":101,\"menuType\":\"C\",\"perms\":\"system:role:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:30:59');
INSERT INTO `sys_oper_log` VALUES (244, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"tree\",\"orderNum\":\"5\",\"menuName\":\"部门管理\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"dept\",\"component\":\"system/dept/index\",\"children\":[],\"createTime\":1666097934000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":103,\"menuType\":\"C\",\"perms\":\"system:dept:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:34:35');
INSERT INTO `sys_oper_log` VALUES (245, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"message\",\"orderNum\":\"5\",\"menuName\":\"通知公告\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"notice\",\"component\":\"system/notice/index\",\"children\":[],\"createTime\":1666097934000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":107,\"menuType\":\"C\",\"perms\":\"system:notice:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:34:48');
INSERT INTO `sys_oper_log` VALUES (246, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"post\",\"orderNum\":\"4\",\"menuName\":\"岗位管理\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"post\",\"component\":\"system/post/index\",\"children\":[],\"createTime\":1666097934000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":104,\"menuType\":\"C\",\"perms\":\"system:post:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:34:59');
INSERT INTO `sys_oper_log` VALUES (247, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"tree-table\",\"orderNum\":\"5\",\"menuName\":\"菜单管理\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"menu\",\"component\":\"system/menu/index\",\"children\":[],\"createTime\":1666097934000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":102,\"menuType\":\"C\",\"perms\":\"system:menu:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:35:36');
INSERT INTO `sys_oper_log` VALUES (248, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"message\",\"orderNum\":\"4\",\"menuName\":\"通知公告\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"notice\",\"component\":\"system/notice/index\",\"children\":[],\"createTime\":1666097934000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":107,\"menuType\":\"C\",\"perms\":\"system:notice:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:36:45');
INSERT INTO `sys_oper_log` VALUES (249, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"post\",\"orderNum\":\"5\",\"menuName\":\"岗位管理\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"post\",\"component\":\"system/post/index\",\"children\":[],\"createTime\":1666097934000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":104,\"menuType\":\"C\",\"perms\":\"system:post:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:36:54');
INSERT INTO `sys_oper_log` VALUES (250, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"message\",\"orderNum\":\"4\",\"menuName\":\"通知公告管理\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"notice\",\"component\":\"system/notice/index\",\"children\":[],\"createTime\":1666097934000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":107,\"menuType\":\"C\",\"perms\":\"system:notice:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:37:20');
INSERT INTO `sys_oper_log` VALUES (251, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"tree-table\",\"orderNum\":\"8\",\"menuName\":\"菜单管理\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"menu\",\"component\":\"system/menu/index\",\"children\":[],\"createTime\":1666097934000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":102,\"menuType\":\"C\",\"perms\":\"system:menu:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:38:02');
INSERT INTO `sys_oper_log` VALUES (252, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"tree\",\"orderNum\":\"7\",\"menuName\":\"部门管理\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"dept\",\"component\":\"system/dept/index\",\"children\":[],\"createTime\":1666097934000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":103,\"menuType\":\"C\",\"perms\":\"system:dept:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:38:09');
INSERT INTO `sys_oper_log` VALUES (253, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"post\",\"orderNum\":\"7\",\"menuName\":\"岗位管理\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"post\",\"component\":\"system/post/index\",\"children\":[],\"createTime\":1666097934000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":104,\"menuType\":\"C\",\"perms\":\"system:post:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:38:24');
INSERT INTO `sys_oper_log` VALUES (254, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"list\",\"orderNum\":\"6\",\"menuName\":\"检查费用设置\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"examCost\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:40:29');
INSERT INTO `sys_oper_log` VALUES (255, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"tree\",\"orderNum\":\"1\",\"menuName\":\"科室管理\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"dept\",\"component\":\"system/depts/index\",\"children\":[],\"createTime\":1666182495000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2025,\"menuType\":\"C\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:41:14');
INSERT INTO `sys_oper_log` VALUES (256, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"tree\",\"orderNum\":\"1\",\"menuName\":\"科室管理\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"depts\",\"component\":\"system/depts/index\",\"children\":[],\"createTime\":1666182495000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2025,\"menuType\":\"C\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:41:21');
INSERT INTO `sys_oper_log` VALUES (257, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"list\",\"orderNum\":\"6\",\"menuName\":\"检查费用设置\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"cost\",\"component\":\"system/cost/index\",\"children\":[],\"createTime\":1666183229000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2026,\"menuType\":\"C\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:41:57');
INSERT INTO `sys_oper_log` VALUES (258, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"list\",\"orderNum\":\"6\",\"menuName\":\"检查费用设置\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"exam\",\"component\":\"system/exam/index\",\"children\":[],\"createTime\":1666183229000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2026,\"menuType\":\"C\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:42:35');
INSERT INTO `sys_oper_log` VALUES (259, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"tree\",\"orderNum\":\"10\",\"menuName\":\"部门管理\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"dept\",\"component\":\"system/dept/index\",\"children\":[],\"createTime\":1666097934000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":103,\"menuType\":\"C\",\"perms\":\"system:dept:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:42:45');
INSERT INTO `sys_oper_log` VALUES (260, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"post\",\"orderNum\":\"10\",\"menuName\":\"岗位管理\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"post\",\"component\":\"system/post/index\",\"children\":[],\"createTime\":1666097934000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":104,\"menuType\":\"C\",\"perms\":\"system:post:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:42:53');
INSERT INTO `sys_oper_log` VALUES (261, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"edit\",\"orderNum\":\"11\",\"menuName\":\"参数设置\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"config\",\"component\":\"system/config/index\",\"children\":[],\"createTime\":1666097934000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":106,\"menuType\":\"C\",\"perms\":\"system:config:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:43:00');
INSERT INTO `sys_oper_log` VALUES (262, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"tree-table\",\"orderNum\":\"10\",\"menuName\":\"菜单管理\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"menu\",\"component\":\"system/menu/index\",\"children\":[],\"createTime\":1666097934000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":102,\"menuType\":\"C\",\"perms\":\"system:menu:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:43:11');
INSERT INTO `sys_oper_log` VALUES (263, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"list\",\"orderNum\":\"7\",\"menuName\":\"挂号费用设置\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"regis\",\"component\":\"system/regis/index\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:45:09');
INSERT INTO `sys_oper_log` VALUES (264, '通知公告', 2, 'com.ruoyi.project.system.controller.SysNoticeController.edit()', 'PUT', 1, 'admin', NULL, '/system/notice', '127.0.0.1', '内网IP', '{\"noticeType\":\"2\",\"remark\":\"管理员\",\"params\":{},\"noticeId\":1,\"noticeTitle\":\"温馨提醒：新版本发布啦\",\"noticeContent\":\"<p>新版本内容</p>\",\"createBy\":\"admin\",\"createTime\":1665735596000,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:53:02');
INSERT INTO `sys_oper_log` VALUES (265, '通知公告', 2, 'com.ruoyi.project.system.controller.SysNoticeController.edit()', 'PUT', 1, 'admin', NULL, '/system/notice', '127.0.0.1', '内网IP', '{\"noticeType\":\"1\",\"remark\":\"管理员\",\"params\":{},\"noticeId\":2,\"noticeTitle\":\"维护通知：系统凌晨维护\",\"noticeContent\":\"<p>维护内容</p>\",\"createBy\":\"admin\",\"createTime\":1665735596000,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:53:11');
INSERT INTO `sys_oper_log` VALUES (266, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"clipboard\",\"orderNum\":\"2\",\"menuName\":\"药品进销存管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"drugManage\",\"children\":[],\"createTime\":1665836354000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2004,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:53:36');
INSERT INTO `sys_oper_log` VALUES (267, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"monitor\",\"orderNum\":\"7\",\"menuName\":\"系统监控\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"monitor\",\"children\":[],\"createTime\":1666097934000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:54:14');
INSERT INTO `sys_oper_log` VALUES (268, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"tool\",\"orderNum\":\"6\",\"menuName\":\"系统工具\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"tool\",\"children\":[],\"createTime\":1666097934000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":3,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:54:24');
INSERT INTO `sys_oper_log` VALUES (269, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"list\",\"orderNum\":\"1\",\"menuName\":\"生产厂家维护\",\"params\":{},\"parentId\":2004,\"isCache\":\"0\",\"path\":\"factory\",\"component\":\"drugManage/factory/index\",\"children\":[],\"createTime\":1665836447000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2005,\"menuType\":\"C\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:55:31');
INSERT INTO `sys_oper_log` VALUES (270, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"list\",\"orderNum\":\"2\",\"menuName\":\"药品信息维护\",\"params\":{},\"parentId\":2004,\"isCache\":\"0\",\"path\":\"drug\",\"component\":\"drugManage/drug/index\",\"children\":[],\"createTime\":1665892547000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2024,\"menuType\":\"C\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:55:42');
INSERT INTO `sys_oper_log` VALUES (271, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"list\",\"orderNum\":\"3\",\"menuName\":\"2.3.供应商维护\",\"params\":{},\"parentId\":2004,\"isCache\":\"0\",\"path\":\"provider\",\"component\":\"drugMange/provider/index\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:56:40');
INSERT INTO `sys_oper_log` VALUES (272, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"list\",\"orderNum\":\"3\",\"menuName\":\"2.3.供应商维护\",\"params\":{},\"parentId\":2004,\"isCache\":\"0\",\"path\":\"provider\",\"component\":\"drugManage/provider/index\",\"children\":[],\"createTime\":1666184200000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2028,\"menuType\":\"C\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:56:50');
INSERT INTO `sys_oper_log` VALUES (273, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"list\",\"orderNum\":\"3\",\"menuName\":\"供应商维护\",\"params\":{},\"parentId\":2004,\"isCache\":\"0\",\"path\":\"provider\",\"component\":\"drugManage/provider/index\",\"children\":[],\"createTime\":1666184200000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2028,\"menuType\":\"C\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:57:01');
INSERT INTO `sys_oper_log` VALUES (274, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"list\",\"orderNum\":\"4\",\"menuName\":\"采购入库\",\"params\":{},\"parentId\":2004,\"isCache\":\"0\",\"path\":\"purchase\",\"component\":\"drugManage/purchase/index\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:58:12');
INSERT INTO `sys_oper_log` VALUES (275, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"list\",\"orderNum\":\"5\",\"menuName\":\"入库审核\",\"params\":{},\"parentId\":2004,\"isCache\":\"0\",\"path\":\"examine\",\"component\":\"drugManage/examine/index\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 20:59:16');
INSERT INTO `sys_oper_log` VALUES (276, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"list\",\"orderNum\":\"6\",\"menuName\":\"库存查询\",\"params\":{},\"parentId\":2004,\"isCache\":\"0\",\"path\":\"inventory\",\"component\":\"drugManage/inventory/index\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 21:00:35');
INSERT INTO `sys_oper_log` VALUES (277, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"guide\",\"orderNum\":\"10\",\"menuName\":\"若依官网\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"http://ruoyi.vip\",\"children\":[],\"createTime\":1666097934000,\"updateBy\":\"admin\",\"isFrame\":\"0\",\"menuId\":4,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 21:01:29');
INSERT INTO `sys_oper_log` VALUES (278, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"excel\",\"orderNum\":\"3\",\"menuName\":\"看病就诊\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"doctor\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 21:02:22');
INSERT INTO `sys_oper_log` VALUES (279, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"list\",\"orderNum\":\"1\",\"menuName\":\"门诊挂号\",\"params\":{},\"parentId\":2032,\"isCache\":\"0\",\"path\":\"register\",\"component\":\"doctor/register/index\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 21:04:05');
INSERT INTO `sys_oper_log` VALUES (280, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"list\",\"orderNum\":\"3\",\"menuName\":\"挂号列表\",\"params\":{},\"parentId\":2032,\"isCache\":\"0\",\"path\":\"regislist\",\"component\":\"doctor/regislist/index\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 21:06:47');
INSERT INTO `sys_oper_log` VALUES (281, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"list\",\"orderNum\":\"3\",\"menuName\":\"新开就诊\",\"params\":{},\"parentId\":2032,\"isCache\":\"0\",\"path\":\"newcare\",\"component\":\"doctor/newcare/index\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 21:08:29');
INSERT INTO `sys_oper_log` VALUES (282, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"list\",\"orderNum\":\"4\",\"menuName\":\"我的排班\",\"params\":{},\"parentId\":2032,\"isCache\":\"0\",\"path\":\"mysched\",\"component\":\"doctor/mysched/index\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 21:09:53');
INSERT INTO `sys_oper_log` VALUES (283, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"list\",\"orderNum\":\"5\",\"menuName\":\"医生排班\",\"params\":{},\"parentId\":2032,\"isCache\":\"0\",\"path\":\"sched\",\"component\":\"doctor/sched\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 21:10:57');
INSERT INTO `sys_oper_log` VALUES (284, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"list\",\"orderNum\":\"6\",\"menuName\":\"患者库\",\"params\":{},\"parentId\":2032,\"isCache\":\"0\",\"path\":\"patient\",\"component\":\"doctor/patient/index\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 21:12:08');
INSERT INTO `sys_oper_log` VALUES (285, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"list\",\"orderNum\":\"5\",\"menuName\":\"医生排班\",\"params\":{},\"parentId\":2032,\"isCache\":\"0\",\"path\":\"sched\",\"component\":\"doctor/sched/index\",\"children\":[],\"createTime\":1666185057000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2037,\"menuType\":\"C\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 21:12:19');
INSERT INTO `sys_oper_log` VALUES (286, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"code\",\"orderNum\":\"8\",\"menuName\":\"内容管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"conent\",\"children\":[],\"createTime\":1666097934000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":5,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 21:15:47');
INSERT INTO `sys_oper_log` VALUES (287, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"excel\",\"orderNum\":\"4\",\"menuName\":\"收费管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"charge\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 21:17:54');
INSERT INTO `sys_oper_log` VALUES (288, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"list\",\"orderNum\":\"1\",\"menuName\":\"科室管理\",\"params\":{},\"parentId\":2025,\"isCache\":\"0\",\"path\":\"depts\",\"component\":\"system/depts/index\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 21:19:59');
INSERT INTO `sys_oper_log` VALUES (289, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/2040', '127.0.0.1', '内网IP', '{menuId=2040}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 21:20:19');
INSERT INTO `sys_oper_log` VALUES (290, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"list\",\"orderNum\":\"1\",\"menuName\":\"处方收费\",\"params\":{},\"parentId\":2039,\"isCache\":\"0\",\"path\":\"charge\",\"component\":\"charge/charge\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 21:22:41');
INSERT INTO `sys_oper_log` VALUES (291, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"list\",\"orderNum\":\"2\",\"menuName\":\"收费查询\",\"params\":{},\"parentId\":2039,\"isCache\":\"0\",\"path\":\"chargelist\",\"component\":\"charge/chargelist\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 21:23:36');
INSERT INTO `sys_oper_log` VALUES (292, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"list\",\"orderNum\":\"1\",\"menuName\":\"处方收费\",\"params\":{},\"parentId\":2039,\"isCache\":\"0\",\"path\":\"charge\",\"component\":\"charge/charge/index\",\"children\":[],\"createTime\":1666185761000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2041,\"menuType\":\"C\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 21:23:46');
INSERT INTO `sys_oper_log` VALUES (293, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"list\",\"orderNum\":\"2\",\"menuName\":\"收费查询\",\"params\":{},\"parentId\":2039,\"isCache\":\"0\",\"path\":\"chargelist\",\"component\":\"charge/chargelist/index\",\"children\":[],\"createTime\":1666185816000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2042,\"menuType\":\"C\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 21:23:54');
INSERT INTO `sys_oper_log` VALUES (294, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"list\",\"orderNum\":\"3\",\"menuName\":\"处方退费\",\"params\":{},\"parentId\":2039,\"isCache\":\"0\",\"path\":\"backfee\",\"component\":\"charge/backfee/index\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 21:25:49');
INSERT INTO `sys_oper_log` VALUES (295, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"list\",\"orderNum\":\"4\",\"menuName\":\"退费查询\",\"params\":{},\"parentId\":2039,\"isCache\":\"0\",\"path\":\"backfeelist\",\"component\":\"charge/backfeelist/index\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 21:29:58');
INSERT INTO `sys_oper_log` VALUES (296, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"list\",\"orderNum\":\"5\",\"menuName\":\"处方发药\",\"params\":{},\"parentId\":2039,\"isCache\":\"0\",\"path\":\"dispense\",\"component\":\"charge/dispense/index\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 21:35:13');
INSERT INTO `sys_oper_log` VALUES (297, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"tab\",\"orderNum\":\"5\",\"menuName\":\"检查管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"check\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 21:39:37');
INSERT INTO `sys_oper_log` VALUES (298, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"list\",\"orderNum\":\"1\",\"menuName\":\"新开检查\",\"params\":{},\"parentId\":2046,\"isCache\":\"0\",\"path\":\"docheck\",\"component\":\"check/docheck/index\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 21:42:27');
INSERT INTO `sys_oper_log` VALUES (299, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"list\",\"orderNum\":\"2\",\"menuName\":\"检查结果录入\",\"params\":{},\"parentId\":2046,\"isCache\":\"0\",\"path\":\"checkresult\",\"component\":\"check/checkresult/index\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 21:43:32');
INSERT INTO `sys_oper_log` VALUES (300, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"list\",\"orderNum\":\"3\",\"menuName\":\"检查结果查询\",\"params\":{},\"parentId\":2046,\"isCache\":\"0\",\"path\":\"checklist\",\"component\":\"check/checklist/index\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 21:44:52');
INSERT INTO `sys_oper_log` VALUES (301, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"tool\",\"orderNum\":\"11\",\"menuName\":\"系统工具\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"tool\",\"children\":[],\"createTime\":1666097934000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":3,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 21:45:01');
INSERT INTO `sys_oper_log` VALUES (302, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"druid\",\"orderNum\":\"6\",\"menuName\":\"数据统计\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"statis\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 21:49:15');
INSERT INTO `sys_oper_log` VALUES (303, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"list\",\"orderNum\":\"1\",\"menuName\":\"收支统计\",\"params\":{},\"parentId\":2050,\"isCache\":\"0\",\"path\":\"revenue\",\"component\":\"statis/revenue/index\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 21:50:05');
INSERT INTO `sys_oper_log` VALUES (304, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"list\",\"orderNum\":\"2\",\"menuName\":\"药品销售统计\",\"params\":{},\"parentId\":2050,\"isCache\":\"0\",\"path\":\"sales\",\"component\":\"statis/sales/index\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 21:51:00');
INSERT INTO `sys_oper_log` VALUES (305, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"list\",\"orderNum\":\"3\",\"menuName\":\"检查项目统计\",\"params\":{},\"parentId\":2050,\"isCache\":\"0\",\"path\":\"check\",\"component\":\"statis/check/index\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 21:52:59');
INSERT INTO `sys_oper_log` VALUES (306, '菜单管理', 1, 'com.ruoyi.project.system.controller.SysMenuController.add()', 'POST', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"list\",\"orderNum\":\"4\",\"menuName\":\"工作量统计\",\"params\":{},\"parentId\":2050,\"isCache\":\"0\",\"path\":\"workload\",\"component\":\"statis/workload/index\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-19 21:53:48');
INSERT INTO `sys_oper_log` VALUES (307, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/106', '127.0.0.1', '内网IP', '{menuId=106}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:13:37');
INSERT INTO `sys_oper_log` VALUES (308, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/105', '127.0.0.1', '内网IP', '{menuId=105}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:14:32');
INSERT INTO `sys_oper_log` VALUES (309, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/103', '127.0.0.1', '内网IP', '{menuId=103}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:15:02');
INSERT INTO `sys_oper_log` VALUES (310, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/104', '127.0.0.1', '内网IP', '{menuId=104}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:15:06');
INSERT INTO `sys_oper_log` VALUES (311, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/501', '127.0.0.1', '内网IP', '{menuId=501}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:21:11');
INSERT INTO `sys_oper_log` VALUES (312, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1045', '127.0.0.1', '内网IP', '{menuId=1045}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:21:15');
INSERT INTO `sys_oper_log` VALUES (313, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1044', '127.0.0.1', '内网IP', '{menuId=1044}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:21:18');
INSERT INTO `sys_oper_log` VALUES (314, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1043', '127.0.0.1', '内网IP', '{menuId=1043}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:21:20');
INSERT INTO `sys_oper_log` VALUES (315, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/500', '127.0.0.1', '内网IP', '{menuId=500}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:21:23');
INSERT INTO `sys_oper_log` VALUES (316, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1042', '127.0.0.1', '内网IP', '{menuId=1042}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:21:25');
INSERT INTO `sys_oper_log` VALUES (317, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1041', '127.0.0.1', '内网IP', '{menuId=1041}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:21:28');
INSERT INTO `sys_oper_log` VALUES (318, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1040', '127.0.0.1', '内网IP', '{menuId=1040}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:21:31');
INSERT INTO `sys_oper_log` VALUES (319, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1035', '127.0.0.1', '内网IP', '{menuId=1035}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:21:34');
INSERT INTO `sys_oper_log` VALUES (320, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1034', '127.0.0.1', '内网IP', '{menuId=1034}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:21:37');
INSERT INTO `sys_oper_log` VALUES (321, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1033', '127.0.0.1', '内网IP', '{menuId=1033}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:21:40');
INSERT INTO `sys_oper_log` VALUES (322, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1032', '127.0.0.1', '内网IP', '{menuId=1032}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:21:43');
INSERT INTO `sys_oper_log` VALUES (323, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1031', '127.0.0.1', '内网IP', '{menuId=1031}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:21:45');
INSERT INTO `sys_oper_log` VALUES (324, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1030', '127.0.0.1', '内网IP', '{menuId=1030}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:21:48');
INSERT INTO `sys_oper_log` VALUES (325, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1029', '127.0.0.1', '内网IP', '{menuId=1029}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:21:50');
INSERT INTO `sys_oper_log` VALUES (326, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1028', '127.0.0.1', '内网IP', '{menuId=1028}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:21:53');
INSERT INTO `sys_oper_log` VALUES (327, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1027', '127.0.0.1', '内网IP', '{menuId=1027}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:21:56');
INSERT INTO `sys_oper_log` VALUES (328, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1026', '127.0.0.1', '内网IP', '{menuId=1026}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:21:58');
INSERT INTO `sys_oper_log` VALUES (329, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1025', '127.0.0.1', '内网IP', '{menuId=1025}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:22:00');
INSERT INTO `sys_oper_log` VALUES (330, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1024', '127.0.0.1', '内网IP', '{menuId=1024}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:22:06');
INSERT INTO `sys_oper_log` VALUES (331, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1023', '127.0.0.1', '内网IP', '{menuId=1023}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:22:15');
INSERT INTO `sys_oper_log` VALUES (332, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1022', '127.0.0.1', '内网IP', '{menuId=1022}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:22:19');
INSERT INTO `sys_oper_log` VALUES (333, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1021', '127.0.0.1', '内网IP', '{menuId=1021}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:22:56');
INSERT INTO `sys_oper_log` VALUES (334, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1020', '127.0.0.1', '内网IP', '{menuId=1020}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:22:58');
INSERT INTO `sys_oper_log` VALUES (335, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1019', '127.0.0.1', '内网IP', '{menuId=1019}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:23:00');
INSERT INTO `sys_oper_log` VALUES (336, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1018', '127.0.0.1', '内网IP', '{menuId=1018}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:23:02');
INSERT INTO `sys_oper_log` VALUES (337, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1017', '127.0.0.1', '内网IP', '{menuId=1017}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:23:06');
INSERT INTO `sys_oper_log` VALUES (338, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/118', '127.0.0.1', '内网IP', '{menuId=118}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:23:09');
INSERT INTO `sys_oper_log` VALUES (339, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/117', '127.0.0.1', '内网IP', '{menuId=117}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:23:11');
INSERT INTO `sys_oper_log` VALUES (340, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/116', '127.0.0.1', '内网IP', '{menuId=116}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:23:14');
INSERT INTO `sys_oper_log` VALUES (341, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/115', '127.0.0.1', '内网IP', '{menuId=115}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:23:16');
INSERT INTO `sys_oper_log` VALUES (342, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1060', '127.0.0.1', '内网IP', '{menuId=1060}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:23:19');
INSERT INTO `sys_oper_log` VALUES (343, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1059', '127.0.0.1', '内网IP', '{menuId=1059}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:23:22');
INSERT INTO `sys_oper_log` VALUES (344, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1057', '127.0.0.1', '内网IP', '{menuId=1057}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:23:24');
INSERT INTO `sys_oper_log` VALUES (345, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1058', '127.0.0.1', '内网IP', '{menuId=1058}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:23:26');
INSERT INTO `sys_oper_log` VALUES (346, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1056', '127.0.0.1', '内网IP', '{menuId=1056}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:23:29');
INSERT INTO `sys_oper_log` VALUES (347, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1055', '127.0.0.1', '内网IP', '{menuId=1055}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:23:31');
INSERT INTO `sys_oper_log` VALUES (348, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/114', '127.0.0.1', '内网IP', '{menuId=114}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:23:33');
INSERT INTO `sys_oper_log` VALUES (349, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/113', '127.0.0.1', '内网IP', '{menuId=113}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:23:37');
INSERT INTO `sys_oper_log` VALUES (350, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/112', '127.0.0.1', '内网IP', '{menuId=112}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:23:40');
INSERT INTO `sys_oper_log` VALUES (351, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/111', '127.0.0.1', '内网IP', '{menuId=111}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:23:43');
INSERT INTO `sys_oper_log` VALUES (352, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/110', '127.0.0.1', '内网IP', '{menuId=110}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:23:49');
INSERT INTO `sys_oper_log` VALUES (353, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1049', '127.0.0.1', '内网IP', '{menuId=1049}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:23:53');
INSERT INTO `sys_oper_log` VALUES (354, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1054', '127.0.0.1', '内网IP', '{menuId=1054}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:23:59');
INSERT INTO `sys_oper_log` VALUES (355, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1053', '127.0.0.1', '内网IP', '{menuId=1053}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:24:01');
INSERT INTO `sys_oper_log` VALUES (356, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1052', '127.0.0.1', '内网IP', '{menuId=1052}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:24:03');
INSERT INTO `sys_oper_log` VALUES (357, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1051', '127.0.0.1', '内网IP', '{menuId=1051}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:24:06');
INSERT INTO `sys_oper_log` VALUES (358, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1050', '127.0.0.1', '内网IP', '{menuId=1050}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:24:09');
INSERT INTO `sys_oper_log` VALUES (359, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/109', '127.0.0.1', '内网IP', '{menuId=109}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:24:14');
INSERT INTO `sys_oper_log` VALUES (360, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1048', '127.0.0.1', '内网IP', '{menuId=1048}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:24:24');
INSERT INTO `sys_oper_log` VALUES (361, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1047', '127.0.0.1', '内网IP', '{menuId=1047}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:24:29');
INSERT INTO `sys_oper_log` VALUES (362, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/1046', '127.0.0.1', '内网IP', '{menuId=1046}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:24:34');
INSERT INTO `sys_oper_log` VALUES (363, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/102', '127.0.0.1', '内网IP', '{menuId=102}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:29:11');
INSERT INTO `sys_oper_log` VALUES (364, '用户管理', 2, 'com.ruoyi.project.system.controller.SysUserController.edit()', 'PUT', 1, 'admin', NULL, '/system/user', '127.0.0.1', '内网IP', '{\"roles\":[{\"flag\":false,\"roleId\":2,\"admin\":false,\"dataScope\":\"2\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":false,\"menuCheckStrictly\":false,\"roleKey\":\"common\",\"roleName\":\"普通角色\",\"status\":\"0\"}],\"phonenumber\":\"15666666666\",\"admin\":false,\"loginDate\":1665735596000,\"remark\":\"测试员\",\"delFlag\":\"0\",\"password\":\"\",\"updateBy\":\"admin\",\"postIds\":[2],\"loginIp\":\"127.0.0.1\",\"email\":\"123@qq.com\",\"nickName\":\"张三\",\"sex\":\"1\",\"deptId\":105,\"avatar\":\"\",\"dept\":{\"deptName\":\"测试部门\",\"leader\":\"若依\",\"deptId\":105,\"orderNum\":\"3\",\"params\":{},\"parentId\":101,\"children\":[],\"status\":\"0\"},\"params\":{},\"userName\":\"ry\",\"userId\":2,\"createBy\":\"admin\",\"roleIds\":[2],\"createTime\":1665735596000,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:35:22');
INSERT INTO `sys_oper_log` VALUES (365, '部门管理', 2, 'com.ruoyi.project.system.controller.SysDeptController.edit()', 'PUT', 1, 'admin', NULL, '/system/dept', '127.0.0.1', '内网IP', '{\"deptName\":\"内科\",\"leader\":\"阿鲁\",\"deptId\":100,\"orderNum\":\"0\",\"delFlag\":\"0\",\"params\":{},\"parentId\":0,\"createBy\":\"admin\",\"children\":[],\"createTime\":1665735596000,\"phone\":\"15888888888\",\"updateBy\":\"admin\",\"ancestors\":\"0\",\"email\":\"123@qq.com\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:43:12');
INSERT INTO `sys_oper_log` VALUES (366, '部门管理', 2, 'com.ruoyi.project.system.controller.SysDeptController.edit()', 'PUT', 1, 'admin', NULL, '/system/dept', '127.0.0.1', '内网IP', '{\"deptName\":\"西院区\",\"leader\":\"西院区分管院长\",\"deptId\":100,\"orderNum\":\"0\",\"delFlag\":\"0\",\"params\":{},\"parentId\":0,\"createBy\":\"admin\",\"children\":[],\"createTime\":1665735596000,\"phone\":\"15888888888\",\"updateBy\":\"admin\",\"ancestors\":\"0\",\"email\":\"123@qq.com\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:44:27');
INSERT INTO `sys_oper_log` VALUES (367, '部门管理', 2, 'com.ruoyi.project.system.controller.SysDeptController.edit()', 'PUT', 1, 'admin', NULL, '/system/dept', '127.0.0.1', '内网IP', '{\"deptName\":\"此一级去掉\",\"leader\":\"\",\"deptId\":100,\"orderNum\":\"0\",\"delFlag\":\"0\",\"params\":{},\"parentId\":0,\"createBy\":\"admin\",\"children\":[],\"createTime\":1665735596000,\"phone\":\"15888888888\",\"updateBy\":\"admin\",\"ancestors\":\"0\",\"email\":\"123@qq.com\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:47:21');
INSERT INTO `sys_oper_log` VALUES (368, '部门管理', 2, 'com.ruoyi.project.system.controller.SysDeptController.edit()', 'PUT', 1, 'admin', NULL, '/system/dept', '127.0.0.1', '内网IP', '{\"deptName\":\"西院区\",\"leader\":\"阿鲁\",\"deptId\":101,\"orderNum\":\"1\",\"delFlag\":\"0\",\"params\":{},\"parentId\":100,\"createBy\":\"admin\",\"children\":[],\"createTime\":1665735596000,\"phone\":\"15888888888\",\"updateBy\":\"admin\",\"ancestors\":\"0,100\",\"email\":\"ry@qq.com\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:47:40');
INSERT INTO `sys_oper_log` VALUES (369, '部门管理', 2, 'com.ruoyi.project.system.controller.SysDeptController.edit()', 'PUT', 1, 'admin', NULL, '/system/dept', '127.0.0.1', '内网IP', '{\"deptName\":\"本部\",\"leader\":\"阿鲁\",\"deptId\":102,\"orderNum\":\"2\",\"delFlag\":\"0\",\"params\":{},\"parentId\":100,\"createBy\":\"admin\",\"children\":[],\"createTime\":1665735596000,\"phone\":\"15888888888\",\"updateBy\":\"admin\",\"ancestors\":\"0,100\",\"email\":\"123@qq.com\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:47:56');
INSERT INTO `sys_oper_log` VALUES (370, '部门管理', 2, 'com.ruoyi.project.system.controller.SysDeptController.edit()', 'PUT', 1, 'admin', NULL, '/system/dept', '127.0.0.1', '内网IP', '{\"deptName\":\"内科\",\"leader\":\"\",\"deptId\":103,\"orderNum\":\"1\",\"delFlag\":\"0\",\"params\":{},\"parentId\":101,\"createBy\":\"admin\",\"children\":[],\"createTime\":1665735596000,\"phone\":\"15888888888\",\"updateBy\":\"admin\",\"ancestors\":\"0,100,101\",\"email\":\"123@qq.com\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:48:31');
INSERT INTO `sys_oper_log` VALUES (371, '部门管理', 2, 'com.ruoyi.project.system.controller.SysDeptController.edit()', 'PUT', 1, 'admin', NULL, '/system/dept', '127.0.0.1', '内网IP', '{\"deptName\":\"外科\",\"leader\":\"\",\"deptId\":104,\"orderNum\":\"2\",\"delFlag\":\"0\",\"params\":{},\"parentId\":101,\"createBy\":\"admin\",\"children\":[],\"createTime\":1665735596000,\"phone\":\"15888888888\",\"updateBy\":\"admin\",\"ancestors\":\"0,100,101\",\"email\":\"341@qq.com\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:48:44');
INSERT INTO `sys_oper_log` VALUES (372, '部门管理', 2, 'com.ruoyi.project.system.controller.SysDeptController.edit()', 'PUT', 1, 'admin', NULL, '/system/dept', '127.0.0.1', '内网IP', '{\"deptName\":\"儿科\",\"leader\":\"\",\"deptId\":105,\"orderNum\":\"3\",\"delFlag\":\"0\",\"params\":{},\"parentId\":101,\"createBy\":\"admin\",\"children\":[],\"createTime\":1665735596000,\"phone\":\"15888888888\",\"updateBy\":\"admin\",\"ancestors\":\"0,100,101\",\"email\":\"34@qq.com\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:49:01');
INSERT INTO `sys_oper_log` VALUES (373, '部门管理', 2, 'com.ruoyi.project.system.controller.SysDeptController.edit()', 'PUT', 1, 'admin', NULL, '/system/dept', '127.0.0.1', '内网IP', '{\"deptName\":\"妇科\",\"leader\":\" \",\"deptId\":106,\"orderNum\":\"4\",\"delFlag\":\"0\",\"params\":{},\"parentId\":101,\"createBy\":\"admin\",\"children\":[],\"createTime\":1665735596000,\"phone\":\"15888888888\",\"updateBy\":\"admin\",\"ancestors\":\"0,100,101\",\"email\":\"123@qq.com\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:49:17');
INSERT INTO `sys_oper_log` VALUES (374, '部门管理', 3, 'com.ruoyi.project.system.controller.SysDeptController.remove()', 'DELETE', 1, 'admin', NULL, '/system/dept/107', '127.0.0.1', '内网IP', '{deptId=107}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:49:21');
INSERT INTO `sys_oper_log` VALUES (375, '部门管理', 2, 'com.ruoyi.project.system.controller.SysDeptController.edit()', 'PUT', 1, 'admin', NULL, '/system/dept', '127.0.0.1', '内网IP', '{\"deptName\":\"儿科\",\"leader\":\"\",\"deptId\":108,\"orderNum\":\"1\",\"delFlag\":\"0\",\"params\":{},\"parentId\":102,\"createBy\":\"admin\",\"children\":[],\"createTime\":1665735596000,\"phone\":\"15888888888\",\"updateBy\":\"admin\",\"ancestors\":\"0,100,102\",\"email\":\"345@qq.com\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:49:52');
INSERT INTO `sys_oper_log` VALUES (376, '部门管理', 2, 'com.ruoyi.project.system.controller.SysDeptController.edit()', 'PUT', 1, 'admin', NULL, '/system/dept', '127.0.0.1', '内网IP', '{\"deptName\":\"精神科\",\"leader\":\"\",\"deptId\":109,\"orderNum\":\"2\",\"delFlag\":\"0\",\"params\":{},\"parentId\":102,\"createBy\":\"admin\",\"children\":[],\"createTime\":1665735596000,\"phone\":\"15888888888\",\"updateBy\":\"admin\",\"ancestors\":\"0,100,102\",\"email\":\"134@qq.com\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:50:19');
INSERT INTO `sys_oper_log` VALUES (377, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/2025', '127.0.0.1', '内网IP', '{menuId=2025}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:54:26');
INSERT INTO `sys_oper_log` VALUES (378, '菜单管理', 2, 'com.ruoyi.project.system.controller.SysMenuController.edit()', 'PUT', 1, 'admin', NULL, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"tree\",\"orderNum\":\"1\",\"menuName\":\"科室管理\",\"params\":{},\"parentId\":1,\"isCache\":\"0\",\"path\":\"dept\",\"component\":\"system/dept/index\",\"children\":[],\"createTime\":1666097934000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":103,\"menuType\":\"C\",\"perms\":\"system:dept:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 09:54:38');
INSERT INTO `sys_oper_log` VALUES (379, '菜单管理', 3, 'com.ruoyi.project.system.controller.SysMenuController.remove()', 'DELETE', 1, 'admin', NULL, '/system/menu/102', '127.0.0.1', '内网IP', '{menuId=102}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 10:06:42');
INSERT INTO `sys_oper_log` VALUES (380, '新增生产厂家维护信息', 1, 'com.ruoyi.project.drugManage.controller.FactoryInfoController.add()', 'POST', 1, 'admin', NULL, '/factory/info', '127.0.0.1', '内网IP', '{\"factoryCode\":\"141231\",\"factoryName\":\"哈药集团有限公司\",\"remark\":\"哈尔滨\",\"params\":{},\"createBy\":\"admin\",\"createTime\":1666231981080,\"tel\":\"19023444433\",\"keyword\":\"哈药\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', 0, NULL, '2022-10-20 10:13:01');

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY USING BTREE (`post_id`)
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '岗位信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'ceo', '董事长', 1, '0', 'admin', '2022-10-14 16:19:56', '', NULL, '');
INSERT INTO `sys_post` VALUES (2, 'se', '项目经理', 2, '0', 'admin', '2022-10-14 16:19:56', '', NULL, '');
INSERT INTO `sys_post` VALUES (3, 'hr', '人力资源', 3, '0', 'admin', '2022-10-14 16:19:56', '', NULL, '');
INSERT INTO `sys_post` VALUES (4, 'user', '普通员工', 4, '0', 'admin', '2022-10-14 16:19:56', '', NULL, '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `menu_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint(1) NULL DEFAULT 1 COMMENT '部门树选择项是否关联显示',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY USING BTREE (`role_id`)
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', 1, '1', 1, 1, '0', '0', 'admin', '2022-10-14 16:19:56', '', NULL, '超级管理员');
INSERT INTO `sys_role` VALUES (2, '普通角色', 'common', 2, '2', 0, 0, '0', '0', 'admin', '2022-10-14 16:19:56', 'admin', '2022-10-19 18:13:03', '普通角色');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY USING BTREE (`role_id`, `dept_id`)
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和部门关联表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES (2, 100);
INSERT INTO `sys_role_dept` VALUES (2, 101);
INSERT INTO `sys_role_dept` VALUES (2, 105);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY USING BTREE (`role_id`, `menu_id`)
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (2, 2);
INSERT INTO `sys_role_menu` VALUES (2, 3);
INSERT INTO `sys_role_menu` VALUES (2, 4);
INSERT INTO `sys_role_menu` VALUES (2, 100);
INSERT INTO `sys_role_menu` VALUES (2, 101);
INSERT INTO `sys_role_menu` VALUES (2, 102);
INSERT INTO `sys_role_menu` VALUES (2, 103);
INSERT INTO `sys_role_menu` VALUES (2, 104);
INSERT INTO `sys_role_menu` VALUES (2, 105);
INSERT INTO `sys_role_menu` VALUES (2, 106);
INSERT INTO `sys_role_menu` VALUES (2, 107);
INSERT INTO `sys_role_menu` VALUES (2, 108);
INSERT INTO `sys_role_menu` VALUES (2, 109);
INSERT INTO `sys_role_menu` VALUES (2, 110);
INSERT INTO `sys_role_menu` VALUES (2, 111);
INSERT INTO `sys_role_menu` VALUES (2, 112);
INSERT INTO `sys_role_menu` VALUES (2, 113);
INSERT INTO `sys_role_menu` VALUES (2, 114);
INSERT INTO `sys_role_menu` VALUES (2, 115);
INSERT INTO `sys_role_menu` VALUES (2, 116);
INSERT INTO `sys_role_menu` VALUES (2, 500);
INSERT INTO `sys_role_menu` VALUES (2, 501);
INSERT INTO `sys_role_menu` VALUES (2, 1000);
INSERT INTO `sys_role_menu` VALUES (2, 1001);
INSERT INTO `sys_role_menu` VALUES (2, 1002);
INSERT INTO `sys_role_menu` VALUES (2, 1003);
INSERT INTO `sys_role_menu` VALUES (2, 1004);
INSERT INTO `sys_role_menu` VALUES (2, 1005);
INSERT INTO `sys_role_menu` VALUES (2, 1006);
INSERT INTO `sys_role_menu` VALUES (2, 1007);
INSERT INTO `sys_role_menu` VALUES (2, 1008);
INSERT INTO `sys_role_menu` VALUES (2, 1009);
INSERT INTO `sys_role_menu` VALUES (2, 1010);
INSERT INTO `sys_role_menu` VALUES (2, 1011);
INSERT INTO `sys_role_menu` VALUES (2, 1012);
INSERT INTO `sys_role_menu` VALUES (2, 1013);
INSERT INTO `sys_role_menu` VALUES (2, 1014);
INSERT INTO `sys_role_menu` VALUES (2, 1015);
INSERT INTO `sys_role_menu` VALUES (2, 1016);
INSERT INTO `sys_role_menu` VALUES (2, 1017);
INSERT INTO `sys_role_menu` VALUES (2, 1018);
INSERT INTO `sys_role_menu` VALUES (2, 1019);
INSERT INTO `sys_role_menu` VALUES (2, 1020);
INSERT INTO `sys_role_menu` VALUES (2, 1021);
INSERT INTO `sys_role_menu` VALUES (2, 1022);
INSERT INTO `sys_role_menu` VALUES (2, 1023);
INSERT INTO `sys_role_menu` VALUES (2, 1024);
INSERT INTO `sys_role_menu` VALUES (2, 1025);
INSERT INTO `sys_role_menu` VALUES (2, 1026);
INSERT INTO `sys_role_menu` VALUES (2, 1027);
INSERT INTO `sys_role_menu` VALUES (2, 1028);
INSERT INTO `sys_role_menu` VALUES (2, 1029);
INSERT INTO `sys_role_menu` VALUES (2, 1030);
INSERT INTO `sys_role_menu` VALUES (2, 1031);
INSERT INTO `sys_role_menu` VALUES (2, 1032);
INSERT INTO `sys_role_menu` VALUES (2, 1033);
INSERT INTO `sys_role_menu` VALUES (2, 1034);
INSERT INTO `sys_role_menu` VALUES (2, 1035);
INSERT INTO `sys_role_menu` VALUES (2, 1036);
INSERT INTO `sys_role_menu` VALUES (2, 1037);
INSERT INTO `sys_role_menu` VALUES (2, 1038);
INSERT INTO `sys_role_menu` VALUES (2, 1039);
INSERT INTO `sys_role_menu` VALUES (2, 1040);
INSERT INTO `sys_role_menu` VALUES (2, 1041);
INSERT INTO `sys_role_menu` VALUES (2, 1042);
INSERT INTO `sys_role_menu` VALUES (2, 1043);
INSERT INTO `sys_role_menu` VALUES (2, 1044);
INSERT INTO `sys_role_menu` VALUES (2, 1045);
INSERT INTO `sys_role_menu` VALUES (2, 1046);
INSERT INTO `sys_role_menu` VALUES (2, 1047);
INSERT INTO `sys_role_menu` VALUES (2, 1048);
INSERT INTO `sys_role_menu` VALUES (2, 1049);
INSERT INTO `sys_role_menu` VALUES (2, 1050);
INSERT INTO `sys_role_menu` VALUES (2, 1051);
INSERT INTO `sys_role_menu` VALUES (2, 1052);
INSERT INTO `sys_role_menu` VALUES (2, 1053);
INSERT INTO `sys_role_menu` VALUES (2, 1054);
INSERT INTO `sys_role_menu` VALUES (2, 1055);
INSERT INTO `sys_role_menu` VALUES (2, 1056);
INSERT INTO `sys_role_menu` VALUES (2, 1057);
INSERT INTO `sys_role_menu` VALUES (2, 1058);
INSERT INTO `sys_role_menu` VALUES (2, 1059);
INSERT INTO `sys_role_menu` VALUES (2, 1060);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '密码',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime NULL DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY USING BTREE (`user_id`)
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户信息表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 103, 'admin', '李四', '00', '213@163.com', '15888888888', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2022-10-20 10:18:26', 'admin', '2022-10-14 16:19:56', '', '2022-10-20 10:18:26', '管理员');
INSERT INTO `sys_user` VALUES (2, 105, 'ce', '张三', '00', '123@qq.com', '15666666666', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2022-10-14 16:19:56', 'admin', '2022-10-14 16:19:56', 'admin', '2022-10-20 09:35:22', '测试员');

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY USING BTREE (`user_id`, `post_id`)
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1);
INSERT INTO `sys_user_post` VALUES (2, 2);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY USING BTREE (`user_id`, `role_id`)
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 2);

SET FOREIGN_KEY_CHECKS = 1;
