/*
 Navicat MySQL Data Transfer

 Source Server         : root
 Source Server Type    : MySQL
 Source Server Version : 80028
 Source Host           : localhost:3306
 Source Schema         : his

 Target Server Type    : MySQL
 Target Server Version : 80028
 File Encoding         : 65001

 Date: 22/10/2022 19:13:47
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for drug_info
-- ----------------------------
DROP TABLE IF EXISTS `drug_info`;
CREATE TABLE `drug_info`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `drug_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '药品名称',
  `drug_code` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '药品编码',
  `produce_factory` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生产单位',
  `drug_type` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '药品类型',
  `recipe_type` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '处方类型',
  `unit` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单位',
  `price` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '处方价格',
  `repertory` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '库存量',
  `pre_value` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '预管值',
  `convert` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '换算量',
  `status` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '状态',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `is_delete` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '是否删除',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of drug_info
-- ----------------------------
INSERT INTO `drug_info` VALUES (1, '阿莫西林', '66666', '云南白药集体股份有限公司', '西药', '西药处方', 'g', '200', '100', '2', '1', '0', '2022-10-21 11:59:24', '2022-10-22 16:20:57', 'admin', 'admin', '1', NULL);
INSERT INTO `drug_info` VALUES (2, '阿胶体', 'Sx10001', '云南白药集体股份有限公司', '中草药', '中药处方', 'g', '2', '200', '80', '1', '0', '2022-10-21 18:59:18', '2022-10-22 16:23:21', 'admin', 'admin', '1', NULL);
INSERT INTO `drug_info` VALUES (3, '白果', 'sx0004', '云南白药集体股份有限公司', '中草药', '中药处方', 'g', '2', '300', '170', '1', '0', '2022-10-21 19:00:56', '2022-10-22 16:23:23', 'admin', 'admin', '1', NULL);
INSERT INTO `drug_info` VALUES (4, '白芍', 'sx0005', '云南白药集体股份有限公司', '中草药', '中药处方', 'g', '2', '300', '200', '1', '0', '2022-10-21 19:04:27', '2022-10-22 16:23:25', 'admin', 'admin', '1', NULL);
INSERT INTO `drug_info` VALUES (5, '白芷', '45623', '云南白药集体股份有限公司', '中草药', '中药处方', 'g', '200', '100', '2', '1', '0', NULL, '2022-10-22 16:25:11', NULL, 'admin', '1', NULL);
INSERT INTO `drug_info` VALUES (6, '阿胶珠胶囊', 'sx45364', '云南白药集体股份有限公司', '中草药', '中药处方', 'g', '2', '200', '80', '1', '0', NULL, '2022-10-22 18:51:24', NULL, 'admin', '1', NULL);
INSERT INTO `drug_info` VALUES (7, '白英', 'sx0006', '云南白药集体股份有限公司', '中草药', '中药处方', 'g', '2', '300', '170', '1', '0', NULL, '2022-10-22 16:23:31', NULL, 'admin', '1', NULL);
INSERT INTO `drug_info` VALUES (8, '白芷', 'sx6722', '云南白药集体股份有限公司', '中草药', '中药处方', 'g', '200', '100', '2', '1', '0', NULL, '2022-10-22 16:24:56', NULL, 'admin', '1', NULL);
INSERT INTO `drug_info` VALUES (9, '阿胶珠颗粒', 'sx0007', '云南白药集体股份有限公司', '中草药', '中药处方', 'g', '2', '200', '80', '1', '0', NULL, '2022-10-22 16:24:20', NULL, 'admin', '1', NULL);

SET FOREIGN_KEY_CHECKS = 1;
