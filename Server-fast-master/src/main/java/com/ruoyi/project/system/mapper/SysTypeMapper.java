package com.ruoyi.project.system.mapper;

import com.ruoyi.project.system.domain.SysType;

import java.util.List;

public interface SysTypeMapper
{

    public List<Long> selectExamTypeById(Long typeId);


    public List<SysType> selectTypeAll();
}
