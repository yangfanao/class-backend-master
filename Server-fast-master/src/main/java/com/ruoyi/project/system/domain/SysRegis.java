package com.ruoyi.project.system.domain;

import com.ruoyi.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


public class SysRegis extends BaseEntity
{
    /**
     * 挂号费id
     */
    private Long regisId;

    /**
     * 挂号费名称
     */
    private String regisName;

    /**
     * 挂号费价格
     */
    private String regisPrice;

    /**
     * 状态（0正常 1停用）
     */
    private String status;

    public Long getRegisId() {
        return regisId;
    }

    public void setRegisId(Long regisId) {
        this.regisId = regisId;
    }

    public String getRegisName() {
        return regisName;
    }

    public void setRegisName(String regisName) {
        this.regisName = regisName;
    }

    public String getRegisPrice() {
        return regisPrice;
    }

    public void setRegisPrice(String regisPrice) {
        this.regisPrice = regisPrice;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("regisId", getRegisId())
                .append("regisName", getRegisName())
                .append("regisPrice", getRegisPrice())
                .append("status", getStatus())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .append("createBy", getCreateBy())
                .append("updateBy", getUpdateBy())
                .append("isDelete", getIsDelete())
                .append("remark", getRemark())
                .toString();
    }
}
