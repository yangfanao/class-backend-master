package com.ruoyi.project.system.controller;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.domain.SysExam;
import com.ruoyi.project.system.service.ISysExamService;
import com.ruoyi.project.system.service.ISysTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 检查费用信息
 *
 * @author ruoyi
 */
@Api("检查费用设置")
@RestController
@RequestMapping("/system/exam")
public class SysExamController extends BaseController
{

    @Autowired
    private ISysExamService examService;
    @Autowired
    private ISysTypeService examTypeService;

    /**
     * 获取检查费用列表
     */
    @ApiOperation("查询检查费用信息")
    @PreAuthorize("@ss.hasPermi('system:exam:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysExam exam)
    {
        startPage();
        List<SysExam> list = examService.selectExamList(exam);
        return getDataTable(list);
    }

    /**
     * 新增检查费用信息
     */
    @ApiOperation("新增检查费用信息")
    @Log(title = "新增检查费用信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Validated @RequestBody SysExam exam)
    {
        exam.setCreateBy(SecurityUtils.getUsername());
        exam.setCreateTime(DateUtils.getNowDate());
        return toAjax(examService.insertExam(exam));
    }

    /**
     * 根据编号获取检查费用信息详细信息
     */
    @GetMapping(value = { "/", "/{examId}" })
    public AjaxResult getInfo(@PathVariable(value = "examId", required = false) Long examId)
    {
        examService.selectExamById(examId);
        AjaxResult ajax = AjaxResult.success();
        ajax.put("types", examTypeService.selectTypeAll());
        if (StringUtils.isNotNull(examId))
        {
            ajax.put(AjaxResult.DATA_TAG, examService.selectExamById(examId));
            ajax.put("typeIds", examTypeService.selectExamTypeById(examId));
        }
        return ajax;
    }

    /**
     * 修改检查费用信息
     */
    @Log(title = "修改检查费用信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Validated @RequestBody SysExam exam)
    {
        exam.setUpdateBy(SecurityUtils.getUsername());
        exam.setUpdateTime(DateUtils.getNowDate());
        System.out.println(exam);
        return toAjax(examService.updateExam(exam));
    }

    /**
     * 删除
     */
    @Log(title = "删除检查费用信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{examIds}")
    public AjaxResult remove(@PathVariable Long[] examIds)
    {
        return toAjax(examService.deleteExamByIds(examIds));
    }
}
