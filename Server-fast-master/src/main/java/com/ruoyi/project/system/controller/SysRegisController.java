package com.ruoyi.project.system.controller;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.system.domain.SysRegis;
import com.ruoyi.project.system.service.ISysRegisService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api("挂号费用")
@RestController
@RequestMapping("/system/regis")
public class SysRegisController extends BaseController
{

    @Autowired
    private ISysRegisService regisService;

    /**
     * 获取挂号费列表
     */
    @GetMapping("/list")
    public TableDataInfo list(SysRegis regis)
    {
        startPage();
        List<SysRegis> regiss = regisService.selectRegisList(regis);
        return getDataTable(regiss);
    }

    @ApiOperation("查询单个挂号费信息")
    @GetMapping(value = "/{regisId}")
    public AjaxResult getInfo(@PathVariable("regisId") Long regisId)
    {
        return AjaxResult.success(regisService.selectRegisById(regisId));
    }

    @ApiOperation("新增挂号费信息")
    @Log(title = "新增挂号费信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Validated @RequestBody SysRegis regis)
    {
        regis.setCreateBy(SecurityUtils.getUsername());
        regis.setCreateTime(DateUtils.getNowDate());
        return toAjax(regisService.insertRegis(regis));
    }

    @ApiOperation("修改挂号费信息")
    @Log(title = "修改挂号费信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysRegis regis)
    {
        regis.setUpdateBy(SecurityUtils.getUsername());
        regis.setUpdateTime(DateUtils.getNowDate());
        return toAjax(regisService.updateRegis(regis));
    }

    @ApiOperation("删除挂号费信息")
    @Log(title = "删除挂号费信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(regisService.deleteRegisByIds(ids));
    }
}
