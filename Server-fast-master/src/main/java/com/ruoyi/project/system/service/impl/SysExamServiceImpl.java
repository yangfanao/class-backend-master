package com.ruoyi.project.system.service.impl;

import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.project.system.domain.SysExam;
import com.ruoyi.project.system.domain.SysExamType;
import com.ruoyi.project.system.domain.SysUser;
import com.ruoyi.project.system.domain.SysUserPost;
import com.ruoyi.project.system.mapper.SysExamMapper;
import com.ruoyi.project.system.mapper.SysExamTypeMapper;
import com.ruoyi.project.system.service.ISysExamService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SysExamServiceImpl implements ISysExamService
{
    private static final Logger log = LoggerFactory.getLogger(SysUserServiceImpl.class);

    @Autowired
    private SysExamMapper examMapper;

    @Autowired
    private SysExamTypeMapper examTypeMapper;

    /**
     * 获取检查费用列表
     */
    @Override
    public List<SysExam> selectExamList(SysExam exam) {
        return examMapper.selectExamList(exam);
    }

    /**
     * 新增检查费用信息
     */
    @Override
    public int insertExam(SysExam exam) {
        return examMapper.insertExam(exam);
    }

    @Override
    public SysExam selectExamById(Long examId) {
        return examMapper.selectExamById(examId);
    }

    /**
     * 修改检查费用信息
     */
    @Override
    public int updateExam(SysExam exam) {
        Long examId = exam.getExamId();

        // 删除项目与类型关联
        examTypeMapper.deleteExamTypeByUserId(examId);
        // 新增项目与类型管理
        insertExamType(exam);
        return examMapper.updateExam(exam);
    }

    /**
     * 删除检查费用信息
     * @param examIds
     * @return
     */
    @Override
    public int deleteExamByIds(Long[] examIds) {
        examTypeMapper.deleteExamType(examIds);
        return examMapper.deleteExamByIds(examIds);
    }

    /**
     * 新增项目与类型管理
     * @param exam
     */
    private void insertExamType(SysExam exam)
    {
        Long[] types = exam.getTypeIds();
        if (StringUtils.isNotNull(types))
        {
            // 新增用户与岗位管理
            List<SysExamType> list = new ArrayList<SysExamType>();
            for (Long typeId : types)
            {
                SysExamType et = new SysExamType();
                et.setExamId(exam.getExamId());
                et.setTypeId(typeId);
                list.add(et);
            }
            if (list.size() > 0)
            {
                examTypeMapper.batchExamType(list);
            }
        }
    }
}
