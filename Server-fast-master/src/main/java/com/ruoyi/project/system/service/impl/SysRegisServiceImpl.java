package com.ruoyi.project.system.service.impl;

import com.ruoyi.project.system.domain.SysRegis;
import com.ruoyi.project.system.mapper.SysRegisMapper;
import com.ruoyi.project.system.service.ISysRegisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysRegisServiceImpl implements ISysRegisService {
    @Autowired
    private SysRegisMapper regisMapper;

    @Override
    public List<SysRegis> selectRegisList(SysRegis regis) {
        return regisMapper.selectRegisList(regis);
    }

    @Override
    public SysRegis selectRegisById(Long regisId) {
        return regisMapper.selectRegisById(regisId);
    }

    @Override
    public int insertRegis(SysRegis regis) {
        return regisMapper.insertRegis(regis);
    }

    @Override
    public int updateRegis(SysRegis regis) {
        return regisMapper.updateRegis(regis);
    }

    @Override
    public int deleteRegisByIds(Long[] ids) {
        return regisMapper.deleteRegisByIds(ids);
    }
}
