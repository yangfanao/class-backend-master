package com.ruoyi.project.system.service;

import com.ruoyi.project.system.domain.SysType;

import java.util.List;

public interface ISysTypeService
{
    /**
     * 根据项目ID查询类型列表
     * @param examId
     * @return
     */
    public List<Long> selectExamTypeById(Long examId);

    /**
     * 查询所有类型
     * @return
     */
    public List<SysType> selectTypeAll();
}
