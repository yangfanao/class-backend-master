package com.ruoyi.project.system.service;

import com.ruoyi.project.system.domain.SysExam;
import com.ruoyi.project.system.domain.SysUser;

import java.util.List;

public interface ISysExamService
{
    /**
     * 获取检查费用列表
     */
    public List<SysExam> selectExamList(SysExam exam);

    /**
     * 新增检查费用信息
     */
    public int insertExam(SysExam exam);

    public SysExam selectExamById(Long examId);

    /**
     * 修改检查费用信息
     */
    public int updateExam(SysExam exam);

    /**
     * 删除检查费用信息
     * @param examIds
     * @return
     */
    public int deleteExamByIds(Long[] examIds);
}
