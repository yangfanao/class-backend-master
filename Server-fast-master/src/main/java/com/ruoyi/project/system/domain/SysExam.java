package com.ruoyi.project.system.domain;

import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.List;

public class SysExam extends BaseEntity
{
    private static final long serialVersionUID = 1L;
    /** 项目费用ID */
    @Excel(name = "项目费用ID", cellType = Excel.ColumnType.NUMERIC, prompt = "项目费用ID")
    private Long examId;

    /** 项目名称 */
    @Excel(name = "项目名称",type = Excel.Type.IMPORT)
    private String examName;

    /** 关键字 */
    @Excel(name = "关键字")
    private String examKey;

    /** 项目单价 */
    @Excel(name = "项目单价")
    private BigDecimal examPrice;

    /** 项目成本 */
    @Excel(name = "项目成本")
    private BigDecimal examCost;

    /** 单位 */
    @Excel(name = "单位")
    private String unit;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    @Excel(name = "是否删除")
    private String isDelete;
    /** 类型 **/
    private List<SysType> types;

    private Long[] typeIds;

    /** 缓存类别ID */
    private Long typeId;

    /** 数据库中的类别ID */
    private Long category;

    public String getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(String isDelete) {
        this.isDelete = isDelete;
    }

    public List<SysType> getTypes() {
        return types;
    }

    public void setTypes(List<SysType> types) {
        this.types = types;
    }

    public Long getExamId() {
        return examId;
    }

    public void setExamId(Long examId) {
        this.examId = examId;
    }

    public String getExamName() {
        return examName;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    public String getExamKey() {
        return examKey;
    }

    public void setExamKey(String examKey) {
        this.examKey = examKey;
    }

    public BigDecimal getExamPrice() {
        return examPrice;
    }

    public void setExamPrice(BigDecimal examPrice) {
        this.examPrice = examPrice;
    }

    public BigDecimal getExamCost() {
        return examCost;
    }

    public void setExamCost(BigDecimal examCost) {
        this.examCost = examCost;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    public Long[] getTypeIds() {
        return typeIds;
    }

    public void setTypeIds(Long[] typeIds) {
        this.typeIds = typeIds;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getCategory() {
        return category;
    }

    public void setCategory(Long category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("examId", getExamId())
                .append("examName", getExamName())
                .append("examKey", getExamKey())
                .append("examPrice", getExamPrice())
                .append("examCost", getExamCost())
                .append("unit", getUnit())
                .append("status", getStatus())
                .append("category", getCategory())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .append("createBy", getCreateBy())
                .append("updateBy", getUpdateBy())
                .append("isDelete", getIsDelete())
                .append("remark", getRemark())
                .append("types", getTypes())
                .toString();
    }
}
