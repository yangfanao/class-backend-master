package com.ruoyi.project.system.controller;

import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.project.system.service.ISysTypeService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 检查费用类型信息
 *
 * @author ruoyi
 */
@Api("检查费用设置")
@RestController
@RequestMapping("/system/examType")
public class SysTypeController extends BaseController
{
    @Autowired
    private ISysTypeService examTypeService;

    /**
     * 根据类型编号获取详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:examType:query')")
    @GetMapping(value = "/{typeId}")
    public AjaxResult getInfo(@PathVariable Long typeId)
    {
        return AjaxResult.success(examTypeService.selectExamTypeById(typeId));
    }
}
