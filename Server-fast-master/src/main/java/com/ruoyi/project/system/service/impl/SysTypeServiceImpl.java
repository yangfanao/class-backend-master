package com.ruoyi.project.system.service.impl;

import com.ruoyi.project.system.domain.SysType;
import com.ruoyi.project.system.mapper.SysTypeMapper;
import com.ruoyi.project.system.service.ISysTypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SysTypeServiceImpl implements ISysTypeService
{
    private static final Logger log = LoggerFactory.getLogger(SysUserServiceImpl.class);
    @Autowired
    private SysTypeMapper examTypeMapper;

    @Override
    public List<Long> selectExamTypeById(Long examId)
    {
        return examTypeMapper.selectExamTypeById(examId);
    }

    /**
     * 查询所有类型
     * @return
     */
    @Override
    public List<SysType> selectTypeAll() {
        return examTypeMapper.selectTypeAll();
    }
}
