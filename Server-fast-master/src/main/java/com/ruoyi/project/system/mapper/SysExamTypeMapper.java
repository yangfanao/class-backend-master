package com.ruoyi.project.system.mapper;

import com.ruoyi.project.system.domain.SysExamType;

import java.util.List;

public interface SysExamTypeMapper {
    public int batchExamType(List<SysExamType> list);

    /**
     * 通过项目ID删除与类型管联
     * @param examId
     * @return
     */
    public int deleteExamTypeByUserId(Long examId);

    public int deleteExamType(Long[] examIds);
}
