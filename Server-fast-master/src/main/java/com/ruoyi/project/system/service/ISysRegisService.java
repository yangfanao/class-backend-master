package com.ruoyi.project.system.service;

import com.ruoyi.project.system.domain.SysRegis;

import java.util.List;

public interface ISysRegisService {

   public List<SysRegis> selectRegisList(SysRegis regis);

    public SysRegis selectRegisById(Long regisId);

    public int insertRegis(SysRegis regis);

    public int updateRegis(SysRegis regis);

    public int deleteRegisByIds(Long[] ids);

}
