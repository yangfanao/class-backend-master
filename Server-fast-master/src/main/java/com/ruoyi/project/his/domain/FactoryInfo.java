package com.ruoyi.project.his.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * 生产厂家维护 factory_info
 *
 * @author cj
 * @date 2022-10-15
 */
public class FactoryInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** $column.columnComment */
    @Excel(name = "厂家名称")
    @NotBlank(message = "厂家名称不能为空")
    @Size(min =0,max=64,message = "厂家名称不能超过64个字符")
    private String factoryName;

    /** $column.columnComment */
    @Excel(name = "厂家编码")
    private String factoryCode;

    /** $column.columnComment */
    @Excel(name = "联系人")
    private String contact;

    /** $column.columnComment */
    @Excel(name = "电话")
    private String tel;

    /** $column.columnComment */
    @Excel(name = "地址")
    private String address;

    /** $column.columnComment */
    @Excel(name = "关键字")
    private String keyword;

    /** $column.columnComment */
    @Excel(name = "状态", readConverterExp = "0=正常,1=使用")
    private String status;

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setFactoryName(String factoryName)
    {
        this.factoryName = factoryName;
    }

    public String getFactoryName()
    {
        return factoryName;
    }
    public void setFactoryCode(String factoryCode)
    {
        this.factoryCode = factoryCode;
    }

    public String getFactoryCode()
    {
        return factoryCode;
    }
    public void setContact(String contact)
    {
        this.contact = contact;
    }

    public String getContact()
    {
        return contact;
    }
    public void setTel(String tel)
    {
        this.tel = tel;
    }

    public String getTel()
    {
        return tel;
    }
    public void setKeyword(String keyword)
    {
        this.keyword = keyword;
    }

    public String getKeyword()
    {
        return keyword;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }
    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("factoryName", getFactoryName())
            .append("factoryCode", getFactoryCode())
            .append("contact", getContact())
            .append("tel", getTel())
            .append("keyword", getKeyword())
            .append("status", getStatus())
            .append("address", getAddress())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .append("isDelete", getIsDelete())
            .append("remark", getRemark())
            .toString();
    }
}
