package com.ruoyi.project.his.service;

import com.ruoyi.project.his.domain.FactoryInfo;

import java.util.List;


/**
 * 生产厂家维护
 *
 * @author cj
 * @date 2022-10-15
 */
public interface IFactoryInfoService
{

//    /**
//     * 校验名称
//     *
//     * @param factory 信息
//     * @return 结果
//     */
//    public String checkFactoryNameUnique(FactoryInfo factory);
//
//    /**
//     * 校验编码
//     *
//     * @param factory 编码
//     * @return 结果
//     */
//    public String checkFactoryCodeUnique(FactoryInfo factory);
    /**
     * 单个查询生产厂家维护
     *
     * @param id 主键
     * @return
     */
    public FactoryInfo selectFactoryInfoById(String id);

    /**
     * 批量查询生产厂家维护
     *
     * @param factoryInfo
     * @return 集合
     */
    public List<FactoryInfo> selectFactoryInfoList(FactoryInfo factoryInfo);

    /**
     * 新增生产厂家维护信息
     *
     * @param factoryInfo
     * @return 结果
     */
    public int insertFactoryInfo(FactoryInfo factoryInfo);

    /**
     * 修改生产厂家维护信息
     *
     * @param factoryInfo
     * @return 结果
     */
    public int updateFactoryInfo(FactoryInfo factoryInfo);

    /**
     * 批量删除生产厂家维护信息
     *
     * @param ids 需要删除的生产厂家维护信息主键集合
     * @return 结果
     */
    public int deleteFactoryInfoByIds(String[] ids);

    /**
     * 删除生产厂家维护信息
     *
     * @param id 主键
     * @return 结果
     */
    public int deleteFactoryInfoById(String id);
}
