package com.ruoyi.project.his.service;

import com.ruoyi.project.his.domain.DrugInfo;

import java.util.List;

/**
 * 药品信息维护
 *
 * @author cj
 * @date 2022-10-21
 */
public interface IDrugInfoService
{
    /*
    * 查询所有药品维护信息
    */
    List<DrugInfo> selectDrugInfoList(DrugInfo drug);

    /*
     * 查询单个药品维护信息
     */
    public DrugInfo selectDrugInfoById(String id);

    /*
    * 导入数据的验证查询单个药品维护信息
    */
    public DrugInfo selectDrugInfoByText(String drugName, String drugCode, String produceFactory);

    /*
    * 新增药品维护信息
    */
    public int insertDrugInfo(DrugInfo drug);

    /*
     * 修改药品维护信息
     */
    public int updateDrugInfo(DrugInfo drug);

    /*
    * 删除药品维护信息
    */
    public int deleteDrugInfoByIds(String[] ids);
    /*
     * 导入信息
     */
    public String importDrug(List<DrugInfo> drugList, boolean updateSupport, DrugInfo drugInfo);

}
