package com.ruoyi.project.his.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.project.his.domain.DrugInfo;
import com.ruoyi.project.his.domain.FactoryInfo;
import com.ruoyi.project.his.service.IFactoryInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 生产厂家维护
 *
 * @author cj
 * @date 2022-10-15
 */
@Api("药品进销存管理")
@RestController
@RequestMapping("/factory/info")
public class FactoryInfoController extends BaseController
{
    @Autowired
    private IFactoryInfoService factoryInfoService;

    /**
     * 查询生产厂家维护
     */
//    @PreAuthorize("@ss.hasPermi('factory:info:list')")
    @ApiOperation("查询生产厂家维护信息")
    @GetMapping("/list")
    public TableDataInfo list(FactoryInfo factory)
    {
        startPage();
        List<FactoryInfo> list = factoryInfoService.selectFactoryInfoList(factory);
        return getDataTable(list);
    }

    /**
     * 导出生产厂家维护
     */
    @ApiOperation("导出生产厂家维护信息")
    @Log(title = "导出生产厂家维护信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, FactoryInfo factory)
    {
        List<FactoryInfo> list = factoryInfoService.selectFactoryInfoList(factory);
        ExcelUtil<FactoryInfo> util = new ExcelUtil<FactoryInfo>(FactoryInfo.class);
        util.exportExcel(response, list, "生产厂家维护信息");
    }

    /**
     * 查询单个生产厂家维护信息
     */
    @ApiOperation("查询单个生产厂家维护信息信息")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(factoryInfoService.selectFactoryInfoById(id));
    }

    /**
     * 新增生产厂家维护
     */
    @ApiOperation("新增生产厂家维护信息")
    @Log(title = "新增生产厂家维护信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Validated @RequestBody FactoryInfo factory)
    {
//        if (UserConstants.NOT_UNIQUE.equals(factoryInfoService.checkPostNameUnique(post)))
//        {
//            return AjaxResult.error("生产厂家'" + factory.getFactoryName() + "'已存在");
//        }
//        else if (UserConstants.NOT_UNIQUE.equals(factoryInfoService.checkPostCodeUnique(post)))
//        {
//            return AjaxResult.error("生产厂家'" + factory.getFactoryName() + "'的厂家编码已存在");
//        }
        factory.setCreateBy(SecurityUtils.getUsername());
        factory.setCreateTime(DateUtils.getNowDate());
        return toAjax(factoryInfoService.insertFactoryInfo(factory));
    }

    /**
     * 修改生产厂家维护
     */
//    @PreAuthorize("@ss.hasPermi('factory:info:edit')")
    @ApiOperation("修改生产厂家维护信息")
    @Log(title = "修改生产厂家维护信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody FactoryInfo factory)
    {
        factory.setUpdateBy(SecurityUtils.getUsername());
        factory.setUpdateTime(DateUtils.getNowDate());
        return toAjax(factoryInfoService.updateFactoryInfo(factory));
    }

    /**
     * 删除生产厂家维护
     */
    /*@ApiOperation("删除指定生产厂家维护信息")
    @Log(title = "删除指定生产厂家维护信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(factoryInfoService.deleteFactoryInfoByIds(ids));
    }*/
    @ApiOperation("删除指定生产厂家维护信息")
    @Log(title = "删除指定生产厂家维护信息", businessType = BusinessType.UPDATE)
    @PutMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(factoryInfoService.deleteFactoryInfoByIds(ids));
    }

    /**
     * 状态修改
     */
    @ApiOperation("状态修改")
    @Log(title = "状态修改", businessType = BusinessType.UPDATE)
    @PutMapping("/changeStatus")
    public AjaxResult changeStatus(@RequestBody FactoryInfo factory)
    {
        factory.setUpdateBy(SecurityUtils.getUsername());
        factory.setUpdateTime(DateUtils.getNowDate());
        return toAjax(factoryInfoService.updateFactoryInfo(factory));
    }
}
