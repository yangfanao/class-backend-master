package com.ruoyi.project.his.service.impl;

import java.util.List;

import com.ruoyi.project.his.domain.FactoryInfo;
import com.ruoyi.project.his.mapper.FactoryInfoMapper;
import com.ruoyi.project.his.service.IFactoryInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 生产厂家维护信息Service业务层处理
 *
 * @author cj
 * @date 2022-10-15
 */
@Service
public class FactoryInfoServiceImpl implements IFactoryInfoService
{
    @Autowired
    private FactoryInfoMapper factoryInfoMapper;

    /**
     * 查询生产厂家维护信息
     *
     * @param id I主键
     * @return
     */
    @Override
    public FactoryInfo selectFactoryInfoById(String id)
    {
        return factoryInfoMapper.selectFactoryInfoById(id);
    }

    /**
     * 查询生产厂家维护信息列表
     *
     * @param factoryInfo
     * @return
     */
    @Override
    public List<FactoryInfo> selectFactoryInfoList(FactoryInfo factoryInfo)
    {
        return factoryInfoMapper.selectFactoryInfoList(factoryInfo);
    }

    /**
     * 新增生产厂家维护信息
     *
     * @param factoryInfo
     * @return 结果
     */
    @Override
    public int insertFactoryInfo(FactoryInfo factoryInfo)
    {
        return factoryInfoMapper.insertFactoryInfo(factoryInfo);
    }

    /**
     * 修改生产厂家维护信息
     *
     * @param factoryInfo
     * @return 结果
     */
    @Override
    public int updateFactoryInfo(FactoryInfo factoryInfo)
    {
        return factoryInfoMapper.updateFactoryInfo(factoryInfo);
    }

    /**
     * 批量删除生产厂家维护信息
     *
     * @param ids 需要删除的生产厂家维护信息主键
     * @return 结果
     */
    @Override
    public int deleteFactoryInfoByIds(String[] ids)
    {
        return factoryInfoMapper.deleteFactoryInfoByIds(ids);
    }

    /**
     * 删除生产厂家维护信息信息
     *
     * @param id
     * @return 结果
     */
    @Override
    public int deleteFactoryInfoById(String id)
    {
        return factoryInfoMapper.deleteFactoryInfoById(id);
    }

}
