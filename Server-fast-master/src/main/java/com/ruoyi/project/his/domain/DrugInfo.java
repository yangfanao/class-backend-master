package com.ruoyi.project.his.domain;

import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class DrugInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    private String id;
    @NotBlank(message = "药品名称不能为空")
    @Size(min =0,max=64,message = "药品名称不能超过64个字符")
    @Excel(name="药品名称")
    private String drugName;
    @Excel(name="药品编码")
    private String drugCode;
    @Excel(name="生产单位")
    private String produceFactory;
    @Excel(name="药品类型")
    private String drugType;
    @Excel(name="处方类型")
    private String recipeType;
    @Excel(name="单位")
    private String unit;
    @Excel(name="处方价格")
    private String price;
    @Excel(name="库存量")
    private String repertory;
    @Excel(name="预管值")
    private String preValue;
    @Excel(name="换算值")
    private String convert;
    @Excel(name="状态",readConverterExp = "0=禁用,1=启用")
    private String status;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getDrugName() {
        return drugName;
    }
    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public String getDrugCode() {
        return drugCode;
    }
    public void setDrugCode(String drugCode) {
        this.drugCode = drugCode;
    }

    public String getProduceFactory() {
        return produceFactory;
    }
    public void setProduceFactory(String produceFactory) {
        this.produceFactory = produceFactory;
    }

    public String getDrugType() {
        return drugType;
    }
    public void setDrugType(String drugType) {
        this.drugType = drugType;
    }

    public String getRecipeType() {
        return recipeType;
    }
    public void setRecipeType(String recipeType) {
        this.recipeType = recipeType;
    }

    public String getUnit() {
        return unit;
    }
    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getPrice() {
        return price;
    }
    public void setPrice(String price) {
        this.price = price;
    }

    public String getRepertory() {
        return repertory;
    }
    public void setRepertory(String repertory) {
        this.repertory = repertory;
    }

    public String getPreValue() {
        return preValue;
    }
    public void setPreValue(String preValue) {
        this.preValue = preValue;
    }

    public String getConvert() {
        return convert;
    }
    public void setConvert(String convert) {
        this.convert = convert;
    }

    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id",getId())
                .append("drugName", getDrugName())
                .append("drugCode", getDrugCode())
                .append("produceFactory", getProduceFactory())
                .append("drugType", getDrugType())
                .append("recipeType", getRecipeType())
                .append("unit", getUnit())
                .append("price", getPrice())
                .append("repertory", getRepertory())
                .append("preValue", getPreValue())
                .append("convert", getConvert())
                .append("status", getStatus())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .append("createBy", getCreateBy())
                .append("updateBy", getUpdateBy())
                .append("isDelete", getIsDelete())
                .append("remark", getRemark())
                .toString();
    }
}
