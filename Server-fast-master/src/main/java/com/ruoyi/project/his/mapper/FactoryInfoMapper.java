package com.ruoyi.project.his.mapper;

import java.util.List;
import com.ruoyi.project.his.domain.FactoryInfo;

/**
 * 生产厂家维护信息Mapper接口
 *
 * @author cj
 * @date 2022-10-15
 */
public interface FactoryInfoMapper
{
    /**
     * 查询生产厂家维护信息
     *
     * @param id 主键
     * @return 生产厂家维护信息
     */
    public FactoryInfo selectFactoryInfoById(String id);

    /**
     * 查询生产厂家维护信息列表
     *
     * @param factoryInfo
     * @return 集合
     */
    public List<FactoryInfo> selectFactoryInfoList(FactoryInfo factoryInfo);

    /**
     * 新增生产厂家维护信息
     *
     * @param factoryInfo
     * @return 结果
     */
    public int insertFactoryInfo(FactoryInfo factoryInfo);

    /**
     * 修改生产厂家维护信息
     *
     * @param factoryInfo
     * @return 结果
     */
    public int updateFactoryInfo(FactoryInfo factoryInfo);

    /**
     * 删除生产厂家维护信息
     *
     * @param id 主键
     * @return 结果
     */
    public int deleteFactoryInfoById(String id);

    /**
     * 批量删除生产厂家维护信息
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteFactoryInfoByIds(String[] ids);
}
