package com.ruoyi.project.his.controller;


import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.framework.web.page.TableDataInfo;
import com.ruoyi.project.his.domain.DrugInfo;
import com.ruoyi.project.his.service.IDrugInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;

@Api("药品进销存管理")
@RestController
@RequestMapping("/drug/info")
public class DrugInfoController extends BaseController
{
    @Autowired
    private IDrugInfoService drugInfoService;
    /**
    * 查询所有药品信息
    */
    @ApiOperation("查询药品维护信息")
    @GetMapping("/list")
    public TableDataInfo list(DrugInfo drug)
    {
        startPage();
        List<DrugInfo> list = drugInfoService.selectDrugInfoList(drug);
        return getDataTable(list);
    }

    /**
     * 查询单个生产厂家维护信息
     */
    @ApiOperation("查询单个药品维护信息")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(drugInfoService.selectDrugInfoById(id));
    }

    /**
     * 新增药品维护信息
     */
    @ApiOperation("新增药品维护信息")
    @Log(title = "新增药品维护信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Validated @RequestBody DrugInfo drug)
    {
        drug.setCreateBy(SecurityUtils.getUsername());
        drug.setCreateTime(DateUtils.getNowDate());
        return toAjax(drugInfoService.insertDrugInfo(drug));
    }

    /**
     * 修改药品维护信息
     */
    @ApiOperation("修改药品维护信息")
    @Log(title = "修改药品维护信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DrugInfo drug)
    {
        drug.setUpdateBy(SecurityUtils.getUsername());
        drug.setUpdateTime(DateUtils.getNowDate());
        return toAjax(drugInfoService.updateDrugInfo(drug));
    }

    /**
     * 删除药品维护信息
     */
    @ApiOperation("删除药品维护信息")
    @Log(title = "删除药品维护信息", businessType = BusinessType.UPDATE)
    @PutMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(drugInfoService.deleteDrugInfoByIds(ids));
    }

    /**
     * 状态修改
     */
    @ApiOperation("状态修改")
    @Log(title = "状态修改", businessType = BusinessType.UPDATE)
    @PutMapping("/changeStatus")
    public AjaxResult changeStatus(@RequestBody DrugInfo drug)
    {
        drug.setUpdateBy(SecurityUtils.getUsername());
        drug.setUpdateTime(DateUtils.getNowDate());
        return toAjax(drugInfoService.updateDrugInfo(drug));
    }

    /*
    * 导出数据
    * */
    @Log(title = "药品信息导出", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DrugInfo drug)
    {
        List<DrugInfo> list = drugInfoService.selectDrugInfoList(drug);
        ExcelUtil<DrugInfo> util = new ExcelUtil<DrugInfo>(DrugInfo.class);
        util.exportExcel(response, list, "药品信息数据");
    }
    /*
    * 导入数据
    */
    @Log(title = "药品信息导入", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<DrugInfo> util = new ExcelUtil<DrugInfo>(DrugInfo.class);
        List<DrugInfo> drugList = util.importExcel(file.getInputStream());
        DrugInfo drugInfo=new DrugInfo();
        String message = drugInfoService.importDrug(drugList, updateSupport, drugInfo);
        return AjaxResult.success(message);
    }
}
