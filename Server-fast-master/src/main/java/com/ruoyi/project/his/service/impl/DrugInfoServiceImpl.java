package com.ruoyi.project.his.service.impl;

import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.project.his.domain.DrugInfo;
import com.ruoyi.project.his.mapper.DrugInfoMapper;
import com.ruoyi.project.his.service.IDrugInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DrugInfoServiceImpl implements IDrugInfoService
{
    private static final Logger log = LoggerFactory.getLogger(DrugInfoServiceImpl.class);
    @Autowired
    private DrugInfoMapper drugInfoMapper;
    /*
     * 查询所有药品维护信息
     */
    @Override
    public List<DrugInfo> selectDrugInfoList(DrugInfo drug) {
        return drugInfoMapper.selectDrugInfoList(drug);
    }

    /*
     * 查询单个药品维护信息
     */
    @Override
    public DrugInfo selectDrugInfoById(String id) {
        return drugInfoMapper.selectDrugInfoById(id);
    }

    @Override
    public DrugInfo selectDrugInfoByText(String drugName,String drugCode,String produceFactory) {
        return drugInfoMapper.selectDrugInfoByText(drugName,drugCode,produceFactory);
    }
    /*
     * 新增药品维护信息
     */
    @Override
    public int insertDrugInfo(DrugInfo drug) {
        return drugInfoMapper.insertDrugInfoList(drug);
    }

    /*
     * 修改药品维护信息
     */
    @Override
    public int updateDrugInfo(DrugInfo drug) {
        return drugInfoMapper.updateDrugInfo(drug);
    }

    /*
     * 删除药品维护信息
     */
    @Override
    public int deleteDrugInfoByIds(String[] ids) {
        return drugInfoMapper.deleteDrugInfoByIds(ids);
    }

    @Override
    public String importDrug(List<DrugInfo> drugList, boolean updateSupport, DrugInfo drugInfos) {
        if (StringUtils.isNull(drugList) || drugList.size() == 0)
        {
            throw new ServiceException("导入数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();

        for (DrugInfo drugInfo : drugList){
            try {
                DrugInfo infos = drugInfoMapper.selectDrugInfoByText(drugInfo.getDrugName(),
                        drugInfo.getDrugCode(),drugInfo.getProduceFactory());
                if (StringUtils.isNull(infos)){
                    this.insertDrugInfo(drugInfo);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、数据 " + drugInfo.getDrugName() + " 导入成功");
                }
                else if (updateSupport)
                {
                    this.updateDrugInfo(drugInfo);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、数据 " + drugInfo.getDrugName() + " 更新成功");
                }
                else
                {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、数据 " + drugInfo.getDrugName() + " 已存在");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum +  "、数据 " + drugInfo.getDrugName()  + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0)
        {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        }
        else
        {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }


}
