package com.ruoyi.project.his.mapper;

import com.ruoyi.project.his.domain.DrugInfo;

import java.util.List;

public interface DrugInfoMapper
{
    /*
     * 查询所有药品维护信息
     */
    List<DrugInfo> selectDrugInfoList(DrugInfo drug);

    /*
     * 查询单个药品维护信息
     */
    public DrugInfo selectDrugInfoById(String id);

    /*
     * 新增药品维护信息
     */
    public int insertDrugInfoList(DrugInfo drug);

    /*
     * 修改药品维护信息
     */
    public int updateDrugInfo(DrugInfo drug);

    /*
     * 删除药品维护信息
     */
    public int deleteDrugInfoByIds(String[] ids);

    DrugInfo selectDrugInfoByText(String drugName, String drugCode, String produceFactory);
}
