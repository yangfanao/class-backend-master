import request from '@/utils/request'
import {praseStrEmpty} from "@/utils/ruoyi";

// 查询列表
export function listExam(query) {
  return request({
    url: '/system/exam/list',
    method: 'get',
    params: query
  })
}

// 查询
export function getExam(examId) {
  return request({
    url: '/system/exam/' + praseStrEmpty(examId),
    method: 'get'
  })
}

// 新增
export function addExam(data) {
  return request({
    url: '/system/exam',
    method: 'post',
    data: data
  })
}

// 修改
export function updateExam(data) {
  return request({
    url: '/system/exam',
    method: 'put',
    data: data
  })
}

// 删除用户
export function delExam(examIds) {
  return request({
    url: '/system/exam/' + examIds,
    method: 'delete'
  })
}
