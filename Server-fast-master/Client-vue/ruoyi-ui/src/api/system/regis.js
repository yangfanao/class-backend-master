import request from '@/utils/request'

// 查询挂号费用列表
export function listRegis(query) {
  return request({
    url: '/system/regis/list',
    method: 'get',
    params: query
  })
}

// 查询挂号费用详细
export function getRegis(regisId) {
  return request({
    url: '/system/regis/' + regisId,
    method: 'get'
  })
}


// 新增挂号费用
export function addRegis(data) {
  return request({
    url: '/system/regis',
    method: 'post',
    data: data
  })
}

// 修改挂号费用
export function updateRegis(data) {
  return request({
    url: '/system/regis',
    method: 'put',
    data: data
  })
}

// 删除挂号费用
export function delRegis(regisId) {
  return request({
    url: '/system/regis/' + regisId,
    method: 'delete'
  })
}

