import request from '@/utils/request'

// 批量查询药品维护信息
export function listInfo(query) {
  return request({
    url: '/drug/info/list',
    method: 'get',
    params: query
  })
}

// 查询药品维护信息
export function getInfo(id) {
  return request({
    url: '/drug/info/' + id,
    method: 'get'
  })
}

// 新增药品维护信息
export function addInfo(data) {
  return request({
    url: '/drug/info',
    method: 'post',
    data: data
  })
}

// 修改药品维护信息
export function updateInfo(data) {
  return request({
    url: '/drug/info',
    method: 'put',
    data: data
  })
}

// 删除药品维护信息
export function delInfo(id) {
  return request({
    url: '/drug/info/' + id,
    method: 'put'
  })
}

//状态修改
export function changeStatus(id, status) {
  const data = {
    id,
    status
  }
  return request({
    url: '/drug/info/changeStatus',
    method: 'put',
    data: data
  })
}
