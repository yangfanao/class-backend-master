import request from '@/utils/request'

// 批量查询生产厂家维护信息
export function listInfo(query) {
  return request({
    url: '/factory/info/list',
    method: 'get',
    params: query
  })
}

// 查询单个生产厂家维护信息
export function getInfo(id) {
  return request({
    url: '/factory/info/' + id,
    method: 'get'
  })
}

// 新增生产厂家维护信息
export function addInfo(data) {
  return request({
    url: '/factory/info',
    method: 'post',
    data: data
  })
}

// 修改生产厂家维护信息
export function updateInfo(data) {
  return request({
    url: '/factory/info',
    method: 'put',
    data: data
  })
}

// 删除生产厂家维护信息
export function delInfo(id) {
  return request({
    url: '/factory/info/' + id,
    method: 'put'
  })
}

//状态修改
export function changeStatus(id, status) {
  const data = {
    id,
    status
  }
  return request({
    url: '/factory/info/changeStatus',
    method: 'put',
    data: data
  })
}
